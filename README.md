# README #

### What is this repository for? ###

* Quick summary

This is a mobile application in development for the Software Engineering Project module of Curtin University. The application is made for the client Colombo TV who require a social platform to supplement their satellite TV offering.


* Technologies used
The primary development framework is AngularJS. Ionic Framework is used to build the mobile application, and firebase is used to provide backend services with the database. 

* Development Team
The development team consists of 4 members following the Software Engineering Project module. The project follows agile SCRUM methodology to manage the project.

### How do I get set up? ###

* Summary of set up
The ionic framework needs to be setup using the node package manager to build the application. . The database key is already set to work with the the Firebase database located at `https:\\colombo.firebaseio.com`. Thereafter, a web view can be obtained through the `ionic serve` command in the project directory.