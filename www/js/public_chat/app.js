// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'firebase', 'starter.controllers','starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
      .state('app.home', {
        url: '/home',
        views: {
          'menuContent': {
            templateUrl: 'templates/home.html',
          }
        }
      })

      .state('app.chat_with_colombotv', {
        url: '/chat_with_colombotv',
        views: {
          'menuContent': {
            templateUrl: 'templates/chat_with_colombotv.html',
            controller:'group'
          }
        }
      })

        .state('app.creat_chat_group', {
        url: '/creat_chat_group',
        views: {
          'menuContent': {
            templateUrl: 'templates/creat_chat_group.html',
            controller:'group'
          }
        }
      })

          .state('app.search_chat_group', {
        url: '/search_chat_group',
        views: {
          'menuContent': {
            templateUrl: 'templates/search_chat_group.html',
            controller:'group'
          }
        }
      })


         .state('app.chat_room', {
    url: '/chat_room/{groupID},{groupName}',
    views: {
      'menuContent': {
        templateUrl: 'templates/chat_room.html',
          controller:'chatRoomCtrl'
        
      }
    }
  });


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});
