angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});


})
 //===========================================================Notification Calendar===========================================

   .controller('notifications', function($scope, $ionicModal) {
           date1 = new Date ();
           date = date1.getDate();
         //year = date1.getFullYear();
        // month = date1.getMonth()



            //retrieve data from firebase
            var ref = new Firebase("https://scorching-inferno-3591.firebaseio.com/calendar_events");
            var count=0;
            ref.once("value", function(snapshot)
            {
                $scope.items = [];
                var list=[];//declare an array
                snapshot.forEach(function (snap)//approach loop wise
                {
                    var te=snap.val();
                    var dd=te.date;
                    var tit=te.e_title;

                    //get date through split function
                    var date_split = dd.split("/");
                    var eveDate = date_split[0];
                    //get remaining date from event date
                    var diff=eveDate-date;
                    if(diff==0) {
                        list.push(te);
                        count++;
                    }

                    if(diff==1) {
                        list.push(te);
                        count++;
                    }

                    if(diff==2) {
                        list.push(te);
                        count++;
                    }

                    if(diff==3) {
                        list.push(te);
                        count++;
                    }

                })
                $scope.items=list;//pass list of events into array
                $scope.data = {
                    badgeCount : count
                };
               // $scope.countKK=count;//get number of notifications
            });


   })

    .controller('group', function($scope,$timeout, $ionicModal, $timeout, groupFac, groupDetFac,$ionicPopup, $rootScope, $state, $ionicListDelegate, $filter,$firebaseArray, Users,$localStorage,userJoinedGroups){

        /* Google Analytics: Track view */
        if(typeof analytics !== 'undefined') { analytics.trackView("Public chat view"); }

        $scope.groups = groupFac;//get all the groups details
        $scope.username = Users.getAuth().password.email;
        $scope.usernamee = $localStorage.name;//current log in user

        //create modal
        $ionicModal.fromTemplateUrl('templates/public_chat/creat_chat_group.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal = modal;
        });
        // Cleanup the modal when we're done with it!
        $scope.closeWin = function() {
            $scope.modal.hide();
        };

        //showAlert
        $scope.showAlert = function(text) {
            var alertPopup = $ionicPopup.alert({
                title: text
            });

        };



        //========================Create new groups====================================================================

        $scope.creatgroup = function(name, description) {
            
            $scope.new_group_id = Math.floor(Math.random() * 5000001);//randomly create groupID

            // adding details into firebase(JSON)
            $scope.groups.$add({
                "group_id": $scope.new_group_id,
                "group_name": name,
                "group_description": description,
                "group_admin": $scope.usernamee
            });

            $scope.addMember($scope.new_group_id);//calling addMember function

            $scope.closeWin();
            $state.go('app.chat_with_colombotv');//back to home
            $scope.showAlert("Group Created successfully ..!");


        };
        var loadGroup=function () {


            //to get details for each user's joined group
            var ref = new Firebase("https://scorching-inferno-3591.firebaseio.com/new");

            ref.orderByChild("group_member").equalTo($scope.usernamee).on("value", function (snapshot)//compare value
            {
                $scope.ngroups = [];
                var li = [];
                snapshot.forEach(function (snap) {
                    var te = snap.val();
                    if (te.status == "joined") {
                        var ref22 = new Firebase("https://scorching-inferno-3591.firebaseio.com/group");

                        ref22.orderByChild("group_id").equalTo(te.group_id).on("child_added", function (snapp) {
                            li.push(snapp.val());
                        });

                    }
                })
                $scope.ngroups = li;

            });
        }
        loadGroup();



//======================ADD MEMEBERS INTO GROUP====================================================

        $scope.demoButton = "join";
        $scope.addMember = function(group_id){
            $scope.members = groupDetFac.getRef(group_id);//get details from group_details table

            // add member to group_details table
            $scope.members.$add({
                "group_member":$scope.usernamee,
                "status": "unjoin"
            });
            var firebaseObj = new Firebase("https://scorching-inferno-3591.firebaseio.com/new");
            firebaseObj.push({"group_id":group_id,
                "group_member":$scope.usernamee,
                "status": "unjoin"
            });

        };
        // disable button after joining
        var refme = new Firebase("https://scorching-inferno-3591.firebaseio.com/new");

        refme.once("value", function(snapshot)
        {
            snapshot.forEach(function (nsnap)
            {
                var te=nsnap.val();
                if(te.group_member==$scope.usernamee)
                {
                    if(te.status=="joined"){
                        var id=te.group_id;

                        $timeout(function() {
                            document.getElementById(id).innerText = "joined";

                        }, 1000);
                    }
                }
            })
        });

        //======================JOIN ROOMS=======================================================
        $scope.join_request = function(group_id) {

            //unjoin from public chat room
            if (document.getElementById(group_id).innerText == "joined") {

                var statusRef = new Firebase("https://scorching-inferno-3591.firebaseio.com/new");

                statusRef.orderByChild("group_id").equalTo(group_id).on("child_added", function (snapshot) {
                    var test = snapshot.val();
                    if (test.group_member == $scope.usernamee) {
                        var key = snapshot.key();
                        statusRef.child(key).update({status: "unjoin"});
                        document.getElementById(group_id).innerText = "join";
                        $scope.showAlert("successfully unjoined");
                    }
                });
                //update
                var statusRef2 = new Firebase("https://scorching-inferno-3591.firebaseio.com/group_details/"+ group_id);
                statusRef2.on("child_added", function (snapshot) {
                    var test2 = snapshot.val();
                    if (test2.group_member == $scope.usernamee) {
                        statusRef2.update({status: "unjoin"});
                    }
                });


            }

            //if not join
            else {
                var ref = new Firebase("https://scorching-inferno-3591.firebaseio.com/group_details/" + group_id);
                ref.once("value", function (snapshot) {
                    var a = snapshot.numChildren();
                    //checking number of members
                    if (a < 3) {

                        var statusRef = new Firebase("https://scorching-inferno-3591.firebaseio.com/new");
                        statusRef.orderByChild("group_id").equalTo(group_id).on("child_added", function (snapshot) {
                            var test = snapshot.val();

                            //when create chat room ,after  join to chat room
                            if (test.group_member == $scope.usernamee) {
                                var key = snapshot.key();
                                statusRef.child(key).update({status: "joined"});
                                document.getElementById(group_id).innerText = "joined";
                                //update
                                var statusRef2 = new Firebase("https://scorching-inferno-3591.firebaseio.com/group_details/"+ group_id);
                                statusRef2.on("child_added", function (snapshot) {
                                    var test2 = snapshot.val();
                                    if (test2.group_member == $scope.usernamee) {
                                        statusRef2.update({status: "joined"});
                                    }
                                });

                            }
                            //new members to join chat room ,not a login user
                            else if(test.group_member != $scope.usernamee){

                                $scope.members = groupDetFac.getRef(group_id);//get details from group_details table
                                $scope.members.$add({

                                    "group_member":$scope.usernamee,
                                    "status": "joined"
                                });
                                //pass joined user
                                var firebaseObj = new Firebase("https://scorching-inferno-3591.firebaseio.com/new");
                                firebaseObj.push({"group_id":group_id,
                                    "group_member":$scope.usernamee,
                                    "status": "joined"
                                });
                                document.getElementById(group_id).innerText = "joined";
                            }

                        });

                        $scope.showAlert("successfully joined");//confirmation messages

                    }
                    else {
                        $scope.showAlert("greater than 10 ! You cant joined!");
                    }

                });
            }

        };

        //=========================================DELETE===================================================

        $scope.deleteGroup=function(group_id){
            var statusRef = new Firebase("https://scorching-inferno-3591.firebaseio.com/group");

            statusRef.orderByChild("group_id").equalTo(group_id).on("child_added", function (snapshot) {
                var gkey = snapshot.key();
                statusRef.child(gkey).remove();
            });


            loadGroup();
        }





//=========================================EDIT===================================================
        $scope.edit=function(group){
            $scope.data={ response:group.group_name};
            $ionicPopup.prompt({
                title:"Edit Group Name ",
                scope:$scope
            }).then(function(res){
                if(res!==undefined) group.group_name=$scope.data.response;

            })

        };

    })


    .controller('chatRoomCtrl', function($scope, $ionicModal, $timeout, chatFac, $ionicPopup,$stateParams, Users,$localStorage,$rootScope, $state, $filter,$firebaseArray){

        $scope.group_id = $stateParams.groupID;// get the parameter passed in url
        $scope.gname = $stateParams.groupName;

        $scope.users = Users.getAll();

        // $scope.username = Users.getAuth().password.email;
        $scope.username = $localStorage.name;
        $scope.date = new Date();

        $scope.messages = chatFac.getMsg($scope.group_id); //get message table

        //=================================SEND MESSAGE=================================================================
        $scope.sendMsg = function(Msg , group_id){
            $scope.messages.$add({
                "group_id": group_id,
                "user_name": $scope.username,
                "message": Msg,
                "date": Firebase.ServerValue.TIMESTAMP
            });
            // $scope.input.message="";
        };



        $scope.showAlert = function(text) {
            var alertPopup = $ionicPopup.alert({
                title: text
            });
        };

    })

    .controller('viewgroupdetail', function($scope, $ionicModal, $timeout, groupFac, groupDetFac,$ionicPopup, $rootScope, $state, $ionicListDelegate, $filter,$firebaseArray, Users,$stateParams){

        var gid=$stateParams.groupid;
        function getMemberDetails(group_id) {
            $scope.members = groupDetFac.getRef(group_id);//get details from group_details table
        };

        getMemberDetails(gid);
    });
