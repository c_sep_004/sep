angular.module('starter.services', [])


//==========================================================================================
.factory("groupFac", function($firebaseArray) {
    var groupRef = new Firebase("https://scorching-inferno-3591.firebaseio.com/group");
    return $firebaseArray(groupRef);//it is like 2D array 
})

//===========================================================================================
.factory("groupDetFac", function($firebaseArray) {

  var factory = {};
            
       factory.getRef = function(group_id) {
       var ref = new Firebase("https://scorching-inferno-3591.firebaseio.com/group_details/"+group_id);
       return $firebaseArray(ref);
       };
  
       factory.getNumOfMembers = function(group_id) {
    
                           };
  
     return factory;
})
//========================================================================================================
    .factory("userJoinedGroups", function($firebaseArray) {

        var factory = {};

        factory.getRef = function( usernamee) {
            var useref = new Firebase("https://scorching-inferno-3591.firebaseio.com/userjoinedGroups/"+ usernamee);
            return $firebaseArray(useref);
        };

        //factory.getNumOfMembers = function(group_id) {

       // };

        return factory;
    })
//==============================================================================================
.factory("chatFac", function($firebaseArray){
   
    var factory = {};//create an array

    factory.getMsg = function(group_id) //assign funtions to the array
    {
        var msgRef = new Firebase("https://scorching-inferno-3591.firebaseio.com/messages/"+group_id);
        
        return $firebaseArray(msgRef);
    };
    
    //return array
    return factory;  
});

