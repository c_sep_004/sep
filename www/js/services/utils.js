/** This factory is mainly used to notufy users when there any error messages.
  * @return error messages
  */

angular.module('app').factory('Utils', function($ionicLoading,$ionicPopup) {

/** Under this section,
  * Display and hide loading icon will be done.
  * Display error and notification messages will be done.
  */
var Utils = {

    /* Display loading icon */
    show: function() {
      $ionicLoading.show({
  	    animation: 'fade-in',
  	    showBackdrop: true,
  	    maxWidth: 200,
  	    showDelay: 500,
        template: '<p class="item-icon-left">Loading...<ion-spinner icon="lines"/></p>'
      });
    },

   /* Hide loading icon */
   hide: function(){
      $ionicLoading.hide();
    },

    /* Display user notification messages */
    alertshow: function(gst,msg){
      var alertPopup = $ionicPopup.alert({
				title: gst,
				template: msg
			});
			alertPopup.then(function(res) {
				console.log("error in opening alertbox");
			});
		},

    /* Display error messages */
    errMessage: function(err) 
    {
	    var msg = "Unknown Error...";

	    if(err && err.code) 
        {
	      switch (err.code) 
          {
	        case "EMAIL_TAKEN":
	          msg = "This Email has been taken.";
              break;
	        case "INVALID_EMAIL":
	          msg = "Invalid Email."; 
              break;
            case "NETWORK_ERROR":
	          msg = "Network Error."; 
              break;
	        case "INVALID_PASSWORD":
	          msg = "Invalid Password."; 
              break;
	        case "INVALID_USER":
	          msg = "Invalid username password combination."; 
              break;
	      }
	    }
			Utils.alertshow("Error",msg);
	 },


  };

	return Utils;

});
