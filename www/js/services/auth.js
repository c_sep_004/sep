/** This factory is mainly used to handle user authentication. it handles all  user related functions
  * @return user details
  */

angular.module('app').factory('Auth', function(FURL, $firebaseAuth, $firebaseArray, $firebaseObject, Utils, $cordovaGeolocation, $http, $rootScope,$http) {

  /* Retrieve firebase URL path */   
  var ref = new Firebase(FURL);
  var auth = $firebaseAuth(ref);
  
  /* URL path to json libraries to get user country through google */
  var requestUrl = "http://ip-api.com/json";
  var mainland;
  var gender = "None";
  var age = "None";
  var contact_no = "None";
  var followers_count = "0";
    
    $http.get(requestUrl).success(function(data){
    
     mainland = data.country;
    }).error(function(err){
    
         console.log("Request failed, error= " + err);
    });
    
  /* Retrieve the country 
  $.ajax({
  url: requestUrl,
  type: 'GET',
    success: function(json)
    {
     mainland = json.country;
    },
    error: function(err)
    {
     console.log("Request failed, error= " + err);
    }
  });*/

  /* Function to handle user authentication details */
  var Auth = {
      user: {},

      /* Creating a profile when user registers through application registration */
      createProfile: function(uid, user) 
      {
        /* Retrieve profile deatils */
        var profile = {
				id: uid,
                email: user.email,
                username: user.name,
				registered_in: Date(),
                country: mainland,
                Gender: gender,
                Age: age,
                Contact_no: contact_no,
                image: "none",
                unique_id:"",
                reg_type:"normal",
                followers_count: followers_count
        };

      /* URL path reference to user profile */
      var profileRef = $firebaseArray(ref.child('profile'));
    
      return profileRef.$add(profile).then(function(ref) 
            {
			  var id = ref.key();
			  console.log("added record with id " + id);
			  console.log(profileRef.$indexFor(id)); // returns location in the array
          
              var unique = profileRef.$getRecord(id);
              unique.unique_id = id;
              profileRef.$save(unique);
			});
        
      },

      /** Triggers when user is login through normal login
        * @returrn login status
        */
      login: function(user) 
      {
        return auth.$authWithPassword({email: user.email, password: user.password});
      },

      /** Triggers when user is registering to the system
        * @return registration status
        */
      register: function(user) 
      {
        return auth.$createUser({email: user.email, password: user.password})
        .then(function() {
            
          /* Authenticate user, so we have permission to write to Firebase */
          return Auth.login(user);
        })
        .then(function(data) {
          /* Store user data in Firebase after creating account */
            
          return Auth.createProfile(data.uid, user);
        });
      },

      /* Triggered when user hit sign out tab in side bar */
      logout: function() 
      {
        auth.$unauth();
      },

      /** Reset password function
        * User will be notified if there is a error in the email provided to send the password reset link
        * @return resetpassword status
        */
      resetpassword: function(user) 
      {
        /* Send reset password link through firebase */
		return auth.$resetPassword({email: user.email}).then(function() 
        {
          Utils.alertshow("Success.","The key was sent to your mail.");
        }).catch(function(error) 
                 {
					Utils.errMessage(error);
				 });
      },

      /** Change password function 
        * @return password changed status
        */
      changePassword: function(user) 
      {
        /* Change password through firebase changepassword method */
		return auth.$changePassword({email: user.email, oldPassword: user.oldPass, newPassword: user.newPass});
      },

      /* Check whether the user is already signed in already*/
      signedIn: function() 
      {
        return !!Auth.user.provider; //using !! means (0, undefined, null, etc) = false | otherwise = true
      }
  };

       /* etrieving authentification details through firebase onAuth method */
	   auth.$onAuth(function(authData) 
       {
		if(authData) 
        {
          angular.copy(authData, Auth.user);
          Auth.user.profile = $firebaseObject(ref.child('profile').child(authData.uid));
		} 
        else 
        {
          if(Auth.user && Auth.user.profile) 
          {
            Auth.user.profile.$destroy();
          }

          angular.copy({}, Auth.user);
		}
	   });

	
	return Auth;

});
