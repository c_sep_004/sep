// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', [
    'ionic',
    'app.controllers',
    'app.routes', 'app.services',
    'app.directives',

    'ngStorage',
    'ngCordova',
    'ngMessages',
    //'countrySelect',
    'firebase',
    'angular-toArrayFilter',
    'ion-gallery',

    'programs.controllers',
    'programs.services',
    'programs.directives',

    'messages.module',

    'starter.controllers',
    'starter.services'
])

    .run(function($ionicPlatform, $rootScope, $ionicLoading, $window) {
        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

            $rootScope.show = function (text) {
                $rootScope.loading = $ionicLoading.show({
                    template: text ? text : 'Loading...',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
            };

            $rootScope.notify = function (text) {
                $rootScope.show(text);
                $window.setTimeout(function () {
                    $rootScope.hide()
                }, 1500);
            };

            $rootScope.hide = function () {
                $ionicLoading.hide();
            };

		    //Google Analytics
		    // This is a cordova plugin, does not work with ionic serve
		    // 'analytics' is global variable defined in the plugin
		    if (typeof analytics !== 'undefined') {
		      analytics.startTrackerWithId("UA-84705023-1");
		      console.log('Google Analytics OK');
		    } else {
		      console.log("Google Analytics Unavailable");
		    }
  });
})
