angular.module('messages.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  .state('app.conversations', {
    url: '/conversations',
    views: {
        'menuContent': {
          templateUrl: 'templates/conversations.html',
          controller: 'conversationsCtrl'
        }
      }
  })

  .state('messages', {
    //abstract: true,
    url: '/conversations/:conversationId',
    templateUrl: 'templates/messages.html',
    controller: 'messagesCtrl'
  });

});
