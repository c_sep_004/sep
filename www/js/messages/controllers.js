angular.module('messages.controllers', ['messages.services', 'angularMoment'])

/* This controller handles logic on conversations screen */
.controller('conversationsCtrl', [
	'$scope',
  'Conversations',
	'$firebaseArray',
	'$ionicModal',
	'ConversationsRef',
  'Users',
  '$state',
  '$ionicPopup',
  '$ionicListDelegate',
	function($scope, Conversations, $firebaseArray, $ionicModal, ConversationsRef, Users, $state, $ionicPopup, $ionicListDelegate) {

    /* Google Analytics: Track view */
    if(typeof analytics !== 'undefined') { analytics.trackView("Conversations view"); }

    /* Get conversations for authenticated user */
    var conversations = Conversations.getList();

    $scope.$on("$ionicView.enter", function(event, data){
    connectingPopup = $ionicPopup.show({
        title: 'Connecting...'
      });
    conversations.$loaded(
      function(conversations) {

        $scope.conversations = conversations; // true
        connectingPopup.close();
        $scope.conversations = conversations;
      }, function(error) {
        console.error("Error:", error);

      });
    });

    /* Get logged in user */
    var userData = conversationsRef.root().getAuth();


  $scope.openNewConversationModal = function () {
    $scope.modal.show();
  };

  /* Archive conversation */
  $scope.remove = function (conversationId) {
    Conversations.remove(conversationId);
  };

  /* Block the other user */
  $scope.block = function (conversationId) {
    Conversations.block(conversationId);
    $ionicListDelegate.closeOptionButtons();
  };

  /* Unblock the other user */
  $scope.unblock = function (conversationId) {
    Conversations.unblock(conversationId);
    $ionicListDelegate.closeOptionButtons();
  };

  $ionicModal.fromTemplateUrl('templates/new-conversation.html', {
    scope: $scope
  }).then(function (modal) {
    $scope.modal = modal;
  });

  $scope.$on('$destroy', function () {
    $scope.modal.remove();
  });

  $scope.hideModal = function() {
    $scope.modal.hide();
  };

    $scope.$on("$ionicView.leave", function(event, data){

      $scope.conversations.$destroy();
      console.info("Exit Conversations View");
    });

}])

/* Handles logic on new conversation modal window */
.controller('NewConversationCtrl', [
	'$scope',
	'$firebaseArray',
	'Users',
  'Conversations',
  '$firebaseAuth',
  '$location',
	function($scope, $firebaseArray, Users, Conversations, $firebaseAuth, $location) {

    $scope.users = Users.getAll();

    $scope.thisUserId = Users.getAuth().uid;

    $scope.newConversation = function(userId, name) {
    if (!Conversations.exists(userId)) {
      var id = Conversations.new(userId, name);
      $scope.modal.hide();
      $location.path('/conversations/'+id);
    } else{
      $scope.modal.hide();
      $location.path('/conversations/'+Conversations.getConversationId(userId));
    }
  };

}])

/* Handles logic in one conversation; when messages screen is entered */
.controller('messagesCtrl', [
	'$scope',
	'$stateParams',
	'$firebaseArray',
	'Messages',
  '$timeout',
  '$ionicScrollDelegate',
  '$ionicHistory',
  '$ionicPopup',
  'Users',
  'Conversations',
	function($scope, $stateParams, $firebaseArray, Messages, $timeout, $ionicScrollDelegate, $ionicHistory, $ionicPopup, Users, Conversations) {

    /* Goes back to conversations view */
    /* (this requires menu-toggle in menu view so that history is not cleared) */

    var otherUserId = Conversations.getOtherUserId($stateParams.conversationId);

    $scope.goBack = function() {
      $ionicHistory.goBack();
      console.info("Go back from messages view", $scope.messages.length);

      if ($scope.messages.length===0) {
        console.info("NO MESSAGES!");
      } else if (Conversations.isArchived(otherUserId)){
        console.info("HAS MESSAGES BUT ARCHIVED!");
      } else {
        Conversations.makeActive($stateParams.conversationId);
      }
    };

    $scope.$on("$ionicView.enter", function(event, data){
   // handle event
        console.info("Enter Messages View");
        Conversations.updateSeenStatus($stateParams.conversationId);
    });

  isIOS = ionic.Platform.isWebView() && ionic.Platform.isIOS();

  $scope.messages = Messages.get($stateParams.conversationId);

  /* Set view of messages according keyboard opened or closed */
  $scope.inputUp = function () {
    if (isIOS) {
      cordova.plugins.Keyboard.keyboardHeight = 216;
    }

    scrollBottom(true);
  };
  $scope.inputDown = function () {
    if (isIOS) {
      cordova.plugins.Keyboard.keyboardHeight = 0;
    }

    $ionicScrollDelegate.$getByHandle('chatScroll').resize();
  };

  function scrollBottom(animate) {
    $timeout(function () {
      $ionicScrollDelegate.$getByHandle('chatScroll').scrollBottom(animate);
    }, 300);
  }

  $scope.data = {};
  $scope.sendMessage = function () {
    //var otherUserId = Conversations.getOtherUserId($stateParams.conversationId);
    console.info('otherUserId: ', otherUserId);
    if (_.isEmpty($scope.data.message)) {
      return;
    } else if (Conversations.isBlocked(otherUserId)) {
      console.info('IS BLOCKED');
      $scope.showAlert = function() {
         var alertPopup = $ionicPopup.alert({
           title: 'You have been blocked',
           template: 'You cannot send messages until the other user unblocks you.'
         });
      };
      $scope.showAlert();

      return;
    } else if (Conversations.isBlocker(otherUserId)) {
      console.info('IS BLOCKER');
        $scope.showAlert = function() {
         var alertPopup = $ionicPopup.alert({
           title: 'You have blocked the other user',
           template: 'You cannot send messages until you remove the block'
         });
      };
      $scope.showAlert();

      return;
    }
    else {
      // SEND MESSAGE
      Messages.newMessage($scope.data.message, $stateParams.conversationId);
      //Conversations.setLastMessage($stateParams.conversationId, $scope.data.message);
      $scope.data.message = "";
      Conversations.makeActive($stateParams.conversationId);

      if (Conversations.isArchived(otherUserId)) {
        Conversations.restore(otherUserId);
        //$ionicHistory.removeBackView();
      }
          // Track event for send message
          if(typeof analytics !== 'undefined') { analytics.trackEvent("Messaging", "send_message", "Send Message"); }

    }

  };

    $scope.thisUserId = Users.getAuth().uid;

}]);