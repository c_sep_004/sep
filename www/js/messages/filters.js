angular.module('messages.filters', [])

/* This filter uses momentJS to filter DateTime objects */
.filter('calender', function() {
	return function(time) {
		if (! time) return;

		return moment(time).calendar(null, {
			lastDay: '[Yesterday]',
			sameDay: 'LT',
			lastWeek: 'dddd',
			sameElse: 'DD/MM/YY'
		});

	};
});