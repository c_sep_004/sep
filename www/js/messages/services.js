angular.module('messages.services', [])

/* Provides the firebase reference for messages */
/* FURL is defined in services in root folder as a constant */
/* @returns Firebase reference Object */
.factory('ConversationsRef', ['FURL', function(FURL){
	conversationsRef = new Firebase(FURL+'/conversations');
  return {
    get: function() {
      return conversationsRef;
    }
  };
}])

/* Handles database operations related to conversations */
/* @returns function Object*/
.factory('Conversations', [
  'ConversationsRef',
  '$firebaseArray',
  'Users',
  function(ConversationsRef, $firebaseArray, Users){

    /* Firebase synchronized array contains conversations */
    conversations = $firebaseArray(ConversationsRef.get());

    /* Reference to JSON tree */
    cMessages = ConversationsRef.get().child('conversation-messages');
    cMetadata = ConversationsRef.get().child('conversation-metadata');
    cUsers = ConversationsRef.get().child('users');

    /* ID of authenticated user */
    thisUserId = Users.getAuth().uid;
    thisUserPic = "http://vignette1.wikia.nocookie.net/legomessageboards/images/9/91/Avatar-placeholder.png/revision/latest?cb=20150616201536";

    /* Retrieve name of authenticated user */
    if  (Users.getAuth().provider == 'facebook') {
      thisUserName = Users.getAuth().facebook.displayName;
      thisUserPic = Users.getAuth().facebook.profileImageURL;
    } else if (Users.getAuth().provider == 'password') {
      thisUserName = Users.getAuth().password.email;
      thisUserPic = Users.getAuth().password.profileImageURL;
    } else if (Users.getAuth().provider == 'google') {
      thisUserName = Users.getAuth().google.displayName;
      thisUserPic = Users.getAuth().google.profileImageURL;
    } else {
      thisUserName = 'default';
    }

    return {
      /* Creates a new conversation */
      /* @param userId of other user */
      /* @return reference to new conversation */
      new: function(userId, name) {

        /* Get the name for other user */
      otherUserName = name;


      /* Store conversation details in conversation-metadata with unique ID */
      newConversation = cMetadata.push({
        users: {
          0: thisUserId,
          1: userId
        }
      });

      /* Check if Authenticated user details are already stored */
      userExists = false;
      cUsers.once("value", function(snapshot) {
        userExists = snapshot.hasChild(thisUserId);
      });

      /* If authenticated user details are not already stored, store the relevant details */
      if (!userExists) {
        /* Store user details in users/{user-id}/ */
        cUsers.child(thisUserId).set({
          id: thisUserId,
          name: thisUserName
        }, function(error) {
          if (error) {
            console.info('Error writing to users/');
          }
        });

        /* Store conversation details for authenticated user in users/{user-id}/conversations/{conversation-id}/ */
        cUsers.child(thisUserId).child('conversations').child(newConversation.key()).set({
          id: newConversation.key()
        }, function(error) {
          if (error) {
            console.info("Error writing to ", newUser, "/conversations");
          }
        });
        console.info("thisUserId", thisUserId);
        console.info("userId", userId);
        /* Store details of other user in authenticated user in users/{user-id}/conversation-users/{user-id} */
        cUsers.child(thisUserId).child('conversation-users').child(userId).set({
          conversationId: newConversation.key(),
          id: userId,
          name: otherUserName,
          picture: "http://vignette1.wikia.nocookie.net/legomessageboards/images/9/91/Avatar-placeholder.png/revision/latest?cb=20150616201536",
          active: false
        }, function(error) {
          if (error) {
            console.info("Error writing ", userId, " to ", newUser, "/conversation-users");
          }
        });
        /* If the details for authenticated user already exists, update the details */
      } else {
        /* Update details in users/{userId}/conversations/{conversation-id} */
        cUsers.child(thisUserId).child('conversations').child(newConversation.key()).update({
          id: newConversation.key()
          //name: otherUserName
        }, function(error) {
          if (error) {
            console.info("Error writing to ", newUser, "/conversations");
          }
        });

        /* Update details in users/{userId}/conversation-users/{conversation-id} */
        cUsers.child(thisUserId).child('conversation-users').child(userId).update({
          conversationId: newConversation.key(),
          id: userId,
          name: otherUserName,
          picture: "http://vignette1.wikia.nocookie.net/legomessageboards/images/9/91/Avatar-placeholder.png/revision/latest?cb=20150616201536"
        }, function(error) {
          if (error) {
            console.info("Error writing ", userId, " to ", newUser, "/conversation-users");
          }
        });
      }

      /* Update details for other user in the conversation */
      otherUserExists = false;
      cUsers.once("value", function(snapshot) {
        otherUserExists = snapshot.hasChild(userId);
      });

      /* If details are not stored for other user already, store them. */
      if (!otherUserExists) {
        /* Store details in users/{user-id} */
        cUsers.child(userId).set({
          id: userId,
          name: otherUserName
        }, function(error) {
          if (error) {
            console.info('Error writing to users/');
          }
        });

        /* Store details in users/{user-id}/conversations/{conversation-id} */
        cUsers.child(userId).child('conversations').child(newConversation.key()).set({
          id: newConversation.key()
        }, function(error) {
          if (error) {
            console.info("Error writing to ", newUser, "/conversations");
          }
        });

        /* Store details in users/{user-id}/conversation-users/{user-id} */
        cUsers.child(userId).child('conversation-users').child(thisUserId).set({
          conversationId: newConversation.key(),
          id: thisUserId,
          name: thisUserName,
          picture: thisUserPic,
          active: false
        }, function(error) {
          if (error) {
            console.info("Error writing ", userId, " to ", newUser, "/conversation-users");
          }
        });
        /* If other user details already exist, update the details instead */
      } else {
        /* Update the details in users/{user-id}/conversations/{conversation-id} */
        cUsers.child(userId).child('conversations').child(newConversation.key()).update({
          id: newConversation.key()
          //name: otherUserName
        }, function(error) {
          if (error) {
            console.info("Error writing to ", newUser, "/conversations");
          }
        });
        /* Update the details in users/{user-id}/conversation-users/{user-id} */
        cUsers.child(userId).child('conversation-users').child(thisUserId).update({
          conversationId: newConversation.key(),
          id: thisUserId,
          name: thisUserName,
          picture: thisUserPic
        }, function(error) {
          if (error) {
            console.info("Error writing ", userId, " to ", newUser, "/conversation-users");
          }
        });
      }

      return newConversation.key();

    },

    /* Return all conversation data as a firebaseArray */
    getAll: function() {
     return conversations;
   },
   /* Get firebaseArray of conversations for authenticated user */
   getList: function() {
    var userConversations = cUsers.child(thisUserId).child('conversation-users').orderByChild('active').equalTo(true);

    return $firebaseArray(userConversations);
  },
  /* Get data for one conversation */
  get: function(id) {
    var conversation;
    cMetadata.child(id).once("value", function(data) {
      conversation = data.val();
    });
    return conversation;
  },
  /* Get reference for one conversation*/
  getRef: function(id) {
    return cMetadata.child(id);
  },

  /* Get the user ID of other user in a given conversation */
  getOtherUserId: function(conversationId) {
      var thisUserId = Users.getAuth().uid;
      var otherUserId;

      cMetadata.child(conversationId).child('users').once("value", function(snapshot) {
        var users = snapshot.val();
        if (thisUserId==users[0]) {
          otherUserId = users[1];
        } else {
          otherUserId = users[0];
        }
      });
      return otherUserId;
  },

  /* Get the conversation id between authenticated user and a given user */

  getConversationId: function(userId) {
    var thisUserId = Users.getAuth().uid;
    var conversationId;

    cUsers.child(thisUserId).child('conversation-users').child(userId).child('conversationId').once("value", function(data) {
      conversationId = data.val();
    });
    return conversationId;
  },

  /* Activate conversation to be displayed in covnversations view */
  makeActive: function(conversationId) {
    var thisUserId = Users.getAuth().uid;
    var otherUserId;

    cMetadata.child(conversationId).child('users').once("value", function(snapshot) {
      var users = snapshot.val();
      if (thisUserId == users[0]) {
        otherUserId = users[1];
      } else {
        otherUserId = users[0];
      }
    });

    /* Store details in users/{user-id}/conversation-users/{user-id} */
    cUsers.child(thisUserId).child('conversation-users').child(otherUserId).update({
      active: true
    }, function(error) {
      if (error) {
        console.info("Error activating conversation ", conversationId, " of ", thisUserId);
      }
    });

    /* Store details in users/{user-id}/conversation-users/{user-id} */
    cUsers.child(otherUserId).child('conversation-users').child(thisUserId).update({
      active: true
    }, function(error) {
      if (error) {
        console.info("Error activating conversation ", conversationId, " of ", otherUserId);
      }
    });
  },

  /* Set the last message of a conversation */
  setLastMessage: function(conversationId, text, timestamp) {
   var conversation = conversations.$getRecord(conversationId);
   /* Attempt to update lastMessage only after conversation has been loaded */
      //cUsers.child(thisUserId).child('conversation-user').orderByChild('conversationId').equalTo(conversationId, function() {

        conversation.$update({
          lastMessage: text,
          timestamp: timestamp
        });
      },
      /* Archive chat */
      remove: function (userId) {
       //conversations.$remove(conversationId);
        cUsers.child(thisUserId).child('conversation-users').child(userId).update({
          archive: true,
          active: false
        });
     },
     /* Restore chat from archives */
      restore: function (userId) {
        cUsers.child(thisUserId).child('conversation-users').child(userId).update({
          archive: false
        });
        cUsers.child(userId).child('conversation-users').child(thisUserId).update({
          archive: false
        });
     },

     /* Check if the conversation for a user is archived */
     isArchived: function (userId) {
      var isArchived;
      cUsers.child(thisUserId).child('conversation-users').child(userId).child('archive').once("value", function(data) {
        isArchived = data.val();
      });
      console.info('isArchived: ', isArchived);
      return isArchived;
     },
     delete: function (userId) {
       //conversations.$remove(conversationId);
        cUsers.child(thisUserId).child('conversation-users').child(userId).update({
          archive: true
        });
     },

     /* Block the other user */
     block: function (userId) {
      cUsers.child(userId).child('conversation-users').child(thisUserId).update({
          blocked: true
        });
      cUsers.child(thisUserId).child('conversation-users').child(userId).update({
          blocker: true
        });
     },

     /* Unblock the other user */
     unblock: function (userId) {
      cUsers.child(userId).child('conversation-users').child(thisUserId).update({
          blocked: false
        });
      cUsers.child(thisUserId).child('conversation-users').child(userId).update({
          blocker: false
        });
     },

     /* Check if the authenticated user is blocked by a user */
     isBlocked: function(userId) {
      var isBlocked;
      cUsers.child(thisUserId).child('conversation-users').child(userId).child('blocked').once("value", function(data) {
        isBlocked = data.val();
      });
      console.info('isBlocked: ', isBlocked);
      return isBlocked;
     },

     /* Check if the authenticated user is a blocker for a given user */
      isBlocker: function(userId) {
      var isBlocker;
      cUsers.child(thisUserId).child('conversation-users').child(userId).child('blocker').once("value", function(data) {
        isBlocker = data.val();
        console.info('userId:', userId);
        console.info('thisUserId:', thisUserId);
      });
      console.info('isBlocker: ', isBlocker);
      return isBlocker;
     },

     /* Check whether conversations exists */
     exists: function(userId) {

      var conversations = cUsers.child(thisUserId).child('conversation-users');

      var exists = false;

      conversations.once("value", function(snapshot) {
        exists = snapshot.hasChild(userId);
      });
      return exists;
    },
    updateSeenStatus: function(conversationId) {
      var thisUserId = Users.getAuth().uid;
      var otherUserId;

      cMetadata.child(conversationId).child('users').once("value", function(snapshot) {
        var users = snapshot.val();
        if (thisUserId==users[0]) {
          otherUserId = users[1];
        } else {
          otherUserId = users[0];
        }
        console.info("Update seen status");
        cUsers.child(thisUserId).child('conversation-users').child(otherUserId).child('lastMessage').update({
          seen:true
        });
      });
    }
  };
}])

/* Handles data operations for Messages */
.factory('Messages', ['ConversationsRef', 'Conversations', '$firebaseArray', 'Users', function(ConversationsRef, Conversations, $firebaseArray, Users) {

  var md = ConversationsRef.get().child('conversation-metadata');

  return {
    /* Retrieve messages of a conversation */
    get: function(id) {
     var conversation = Conversations.getRef(id);
      //var messages = conversation.messages;
      var messages = $firebaseArray(conversation.child('messages'));
      return messages;
    },
    /* Create and send new message */
    newMessage: function(text, conversationId) {
      var conversation = md.child(conversationId).child('messages');
			// Get moment object of current timestamp
			var timestamp = moment().format();
			//Conversations.setLastMessage(conversationId, text, timestamp);

      var id = conversation.push({
        text: text,
        timestamp: timestamp,
        userId: Users.getAuth().uid
      }, function(obj) {
        if (obj===null) {
          console.info("Message sent");
        } else {
          console.info("Error sending message", obj);
        }
      });

      /* Set last message of conversation */
      var thisUserId = Users.getAuth().uid;
      var otherUserId;

      md.child(conversationId).child('users').once("value", function(snapshot) {
        var users = snapshot.val();
        if (thisUserId==users[0]) {
          otherUserId = users[1];
        } else {
          otherUserId = users[0];
        }
      });

      /* Set last message for authenticated user */
      ConversationsRef.get().child('users').child(thisUserId).child('conversation-users').child(otherUserId).child('lastMessage').set({
        text:text,
        timestamp: timestamp,
        seen: true
      });
      /* Set last message for other user */
      ConversationsRef.get().child('users').child(otherUserId).child('conversation-users').child(thisUserId).child('lastMessage').set({
        text:text,
        timestamp: timestamp,
        seen: false
      });

    }
  };
}])

/* Handles operations for users */
.factory('Users', ['ConversationsRef', '$firebaseArray', '$localStorage', function(ConversationsRef, $firebaseArray, $localStorage) {
  users = $firebaseArray(ConversationsRef.get().root().child("profile"));

  var authData = ConversationsRef.get().root().getAuth();

  return {
    /* Retrieve data of current authenticated user*/
    getAuth: function() {
      return authData;
    },
    getAll: function () {
      return users;
    },
    getData: function(id) {
      var user;
      ConversationsRef.get().root().child("profile").child(id).once("value", function(data) {
        user = data.val();
      });
      return user;
    }
  };
}])
