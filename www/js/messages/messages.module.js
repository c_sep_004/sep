/* MAJOR REFACTORING NEEDED           */
/*    >Move conversations to factory  */
/*    >Move messages to factory       */
/*    >Integrate with Login           */

angular.module('messages.module', [
	'messages.controllers',
  'messages.routes',
  'messages.directives',
  'messages.services',
  'messages.filters'
])

/*
.factory('Messages', ['$firebaseArray', function($firebaseArray) {

  return {
    get: function(conversationId) {
      var messagesRef = new Firebase("https://brilliant-heat-3455.firebaseio.com/conversation-messages/"+conversationId)
      var messages = $firebaseArray(messagesRef);
      return messages;
    },
    add: function(conversationId, messageContent, sender, sender_name) {
      var messagesRef = new Firebase("https://brilliant-heat-3455.firebaseio.com/conversation-messages/"+conversationId)
      var messages = $firebaseArray(messagesRef);

      messages.$add({
        userId: sender,
        name: sender_name,
        timestamp: moment()
      });
      return true;
    }
  }

}])

.factory('Conversations', ['$firebaseArray', 'FirechatService', function($firebaseArray, FirechatService){

/*
    // Some fake testing data
    var conversations = [
      {
        _id: 0,
        name: 'Ben Sparrow',
        picture: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460',
        lastMessage: {
          text: 'You on your way?',
          timestamp: moment().subtract(1, 'hours')
        },
        messages: [
        	{text: 'Hey', timestamp: moment().subtract(1, 'hours')},
        	{text: 'You on your way?', timestamp: moment().subtract(1, 'hours')}
        ]
      },
      {
        _id: 1,
        name: 'Max Lynx',
        picture: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460',
        lastMessage: {
          text: 'Hey, it\'s me',
          timestamp: moment().subtract(2, 'hours')
        },
        messages: [
        	{text: 'Hey', timestamp: moment().subtract(2, 'hours')},
        	{text: 'Hey, it\'s me', timestamp: moment().subtract(2, 'hours')}
        ]
      },
      {
        _id: 2,
        name: 'Adam Bradleyson',
        picture: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg',
        lastMessage: {
          text: 'I should buy a boat',
          timestamp: moment().subtract(1, 'days')
        },
        messages: [
        	{text: 'Hey', timestamp: moment().subtract(1, 'days')},
        	{text: 'I should buy a boat', timestamp: moment().subtract(1, 'days')}
        ]
      },
      {
        _id: 3,
        name: 'Perry Governor',
        picture: 'https://pbs.twimg.com/profile_images/598205061232103424/3j5HUXMY.png',
        lastMessage: {
          text: 'Look at my mukluks!',
          timestamp: moment().subtract(4, 'days')
        },
        messages: [
        	{text: 'Hey', timestamp: moment().subtract(4, 'days')},
        	{text: 'Look at my mukluks!', timestamp: moment().subtract(4, 'days')}
        ]
      },
      {
        _id: 4,
        name: 'Mike Harrington',
        picture: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460',
        lastMessage: {
          text: 'This is wicked good ice cream.',
          timestamp: moment().subtract(2, 'weeks')
        },
        messages: [
        	{text: 'Hey', timestamp: moment().subtract(2, 'weeks')},
        	{text: 'This is wicked good ice cream.', timestamp: moment().subtract(2, 'weeks')}
        ]
      }
    ];
//
    return {
      all: function() {
        var conversationsRef = new Firebase("https://brilliant-heat-3455.firebaseio.com/messages")
        $scope.conversations = $firebaseArray(conversationsRef);
        return conversations;
      },
      remove: function(conversation) {
        conversations.splice(conversations.indexOf(conversation), 1);
      },
      get: function(conversationId) {
        for (var i = 0; i < conversations.length; i++) {
          if (conversations[i]._id === parseInt(conversationId)) {
            return conversations[i];
          }
        }
        return null;
      }
    };
}])
*/
