angular.module('app.routes', [])

    .config(function($stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/side_menu/menu.html',
                controller:'menuController'
            })

            .state('login', {
                url: '/login',
                templateUrl: 'templates/login/login.html',
                controller:'loginController'
            })
        
            .state('signin', {
                url: '/signin',
                templateUrl: 'templates/login/signin.html',
                controller:'loginController'
            })

            .state('forgot', {
                url: '/forgot',
                templateUrl: 'templates/forgot/forgot.html',
                controller:'forgotController'
            })

            .state('register', {
                url: '/register',
                templateUrl: 'templates/register/register.html',
                controller:'registerController'
            })

            .state('app.home', {
                url: '/home',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home/home.html',
                        controller: 'homeController'
                    }
                }
            })
            .state('app.home.newsfeed', {
                url: '/newsfeed',
                views: {
                    'newsfeed': {
                        templateUrl: 'templates/home/newsfeed.html',
                        controller: 'NewsfeedCtrl'
                    }
                }
            })
            .state('app.home.favourite', {
                url: '/favourite',
                views: {
                    'favourite': {
                        templateUrl: 'templates/home/favourite.html',
                        controller: 'FavouriteCtrl'
                    }
                }
            })
            .state('app.home.nearby', {
                url: '/nearby',
                views: {
                    'nearby': {
                        templateUrl: 'templates/home/nearby.html',
                        controller: 'NearbyCtrl'
                    }
                }
            })
            .state('app.home.map', {
                url: '/nearby/:latitude/:longitude',
                views: {
                    'nearby': {
                        templateUrl: 'templates/home/map.html',
                        controller: 'MapCtrl'
                    }
                }
            })
            .state('app.home.upcoming', {
                url: '/upcoming',
                views: {
                    'upcoming': {
                        templateUrl: 'templates/home/upcoming.html',
                        controller: 'notifications'
                    }
                }
            })

            .state('app.account', {
                url: '/account',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/account/account.html',
                        controller:'editprofileController'
                    }
                }
            })

            .state('app.fbaccount', {
                url: '/fbaccount',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/account/fbaccount.html',
                        controller:'editprofileController'
                    }
                }
            })


            .state('app.users', {
                url: '/users',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/users/users.html',
                        controller:'usersController'
                    }
                }
            })

            .state('app.view_user', {
                url: '/view_user/:unique_id',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/users/view_user.html',
                        controller:'viewuserController'
                    }
                }
            })

            .state('app.view_following', {
                url: '/view_following',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/users/view_following.html',
                        controller:'followingController'
                    }
                }
            })
        
            .state('app.view_other_following', {
                url: '/view_other_following/:user_id',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/users/view_other_following.html',
                        controller:'otherfollowingController'
                    }
                }
            })
        
            .state('app.view_followers', {
                url: '/view_followers',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/users/view_followers.html',
                        controller:'followersController'
                    }
                }
            })
        
            .state('app.view_other_followers', {
                url: '/view_other_followers/:user_id',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/users/view_other_followers.html',
                        controller:'otherfollowersController'
                    }
                }
            })

            .state('app.view_blocked', {
                url: '/view_blocked',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/users/view_blocked.html',
                        controller:'blockedController'
                    }
                }
            })

            .state('app.change_password', {
                url: '/change_password',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/account/change_password.html',
                        controller:'editprofileController'
                    }
                }
            })

            .state('app.change-email', {
                url: '/change_email',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/account/change_email.html',
                        controller:'editprofileController'
                    }
                }
            })
            .state('app.programs', {
                url: '/programs',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/programs/programs.html',
                        controller: 'ProgramsCtrl'
                    }
                }
            })

            .state('app.program', {
                url: '/programs/episodes/:programId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/programs/program.html',
                        controller: 'ProgramEpisodesCtrl'
                    }
                }
            })

            .state('app.episode', {
                url: '/programs/episodes/:programId/:videoId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/programs/episode.html',
                        controller: 'ProgramEpisodeCtrl'
                    }
                }
            })

            .state('app.group', {
                url: '/programs/group/:programId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/programs/group.html',
                        controller: 'ProgramGroupCtrl'
                    }
                }
            })

            //============public chat==========================
            .state('app.chat_with_colombotv', {
                url: '/chat_with_colombotv',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/public_chat/chat_with_colombotv.html',
                        controller:'group'
                    }
                }
            })

            .state('app.creat_chat_group', {
                url: '/creat_chat_group',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/public_chat/creat_chat_group.html',
                        controller:'group'
                    }
                }
            })

            .state('app.search_chat_group', {
                url: '/search_chat_group',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/public_chat/search_chat_group.html',
                        controller:'group'
                    }
                }
            })


            .state('app.chat_room', {
                url: '/chat_room/{groupID},{groupName}',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/public_chat/chat_room.html',
                        controller:'chatRoomCtrl'

                    }
                }
            })

            .state('app.group_member_details', {
                url: '/group_member_details/{groupid}',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/public_chat/group_member_details.html',
                        controller:'viewgroupdetail'

                    }
                }
            })
        $urlRouterProvider.otherwise('/login')

    });
