angular.module('programs.services', [])

    /**
     * Stores the list of programs to display on initial page.
     * Contains three methods to return the programs according to necessity.
     * Contains three other methods to do tasks related to program like functions
     */
    .factory('ProgramsFactory', function ($firebaseObject, $q, $localStorage, $rootScope) {
        //base reference for programs in firebase
        var programRef = new Firebase("https://colombo.firebaseio.com/programs/");
        var programs = $firebaseObject(programRef);

        //base reference for profile in firebase
        var profileRef = new Firebase("https://colombo.firebaseio.com/profile").orderByChild("id").equalTo($localStorage.idd);
        var favourites = $firebaseObject(profileRef);

        return {
            /**
             * Returns the whole programs array
             */
            getAllPrograms: function () {
                return programs;
            },

            /**
             * Returns a specific program JSON object
             * @param programId
             * Checks for a JSON object that matches the parameter
             */
            getProgram: function (programId) {
                var result = {};

                angular.forEach(programs, function (value) {
                    if (value.id == programId) {
                        result = {
                            id: value.id,
                            title: value.title,
                            tags: value.tags.split(','),
                            imageSrc: value.imageSrc,
                            description: value.description.split(' ')
                        };

                        if (value.hasOwnProperty("liked"))
                            result.liked = value.liked;
                    }
                });

                return result;
            },

            /**
             * Returns an array of favourite program IDs
             * Gets the unique key of profile entry and extracts the favourite program IDs
             */
            getFavouriteProgram: function () {
                return favourites.$loaded().then(function (data) {
                    var key = Object.keys(data)[3];
                    var favouriteList = [];

                    angular.forEach(data[key].favourites, function (id) {
                        favouriteList.push(id);
                    });

                    return favouriteList;
                });
            },

            /**
             * Returns boolean value indicating liked or not
             * Gets the unique key of profile entry, checks if "favourites" is available. If available checks whether the
             * program ID sent matches a program ID in favourite list
             * @param programId
             */
            isLiked: function (programId) {
                return favourites.$loaded().then(function (data) {
                    var name = Object.keys(data)[3];
                    var liked = false;

                    if (data[name].hasOwnProperty("favourites")) {
                        angular.forEach(data[name].favourites, function (value) {
                            if (value == programId)
                                liked = true;
                        })
                    }

                    return liked;
                })
            },

            /**
             * Add liked program to firebase
             * @param programId
             */
            like: function (programId) {
                var name = Object.keys(favourites)[3];
                var ref = new Firebase("https://colombo.firebaseio.com/profile/" + name + "/favourites");
                ref.push(programId);
            },

            /**
             * Remove entry with containing programId
             * @param programId
             */
            unlike: function (programId) {
                var name = Object.keys(favourites)[3];

                angular.forEach(favourites[name].favourites, function (value, key) {
                    if (value == programId)
                        delete favourites[name].favourites[key];
                });

                favourites.$save();
            }
        };
    })

    /**
     * Retrieves list of episodes using YouTube API v3.
     * Uses parameters defined in YouTube API documentation to get results matching the request.
     * Contains two methods that retrieves a list of videos and a specific video.
     * Uses $http service to request the API service from YouTube.
     */
    .factory('ProgramFactory', function ($http) {
        // JSON object declaration that contains the parameters to get a list of videos matching the parameters
        var videoListParams = {
            part: 'snippet',
            channelId: 'UCP_kTskrJPfLiYO6ua7OitA',
            maxResults: '',
            order: '',
            pageToken: '',
            q: '',
            type: 'video',
            key: 'AIzaSyARg-AM1ym2C11SkxOLHBAuulZbesMO2Ic'
        };

        // JSON object declaration that contains the parameters to get a unique video matching the parameters
        var videoParams = {
            part: 'snippet,statistics',
            id: '',
            key: 'AIzaSyARg-AM1ym2C11SkxOLHBAuulZbesMO2Ic'
        };

        return {
            /**
             * Retrieves a list of videos using $http.get() request.
             * @param maxResults
             * @param order
             * @param q
             * @param pageToken
             * @success YouTube video ID and Snippet (video thumbnail and basic video details) of each video is extracted
             *      from the response and pushed into an array named videos.
             *      Returns the prevPageToken, nextPageToken and videos array by encapsulating them in a JSON object.
             * @error response will contain necessary data explaining the error that has occurred.
             *      Error code, parameter that caused the error and error message will be printed using the console.log()
             *      method (This is only for development purposes). Returns -1 to indicates that get() request failed.
             */
            getEpisodeList: function (maxResults, order, q, pageToken) {
                videoListParams.maxResults = maxResults;
                videoListParams.order = order;
                videoListParams.q = q;
                videoListParams.pageToken = pageToken;

                return $http.get('https://www.googleapis.com/youtube/v3/search', {params:videoListParams}).then(function (response) {
                    var videos = [];

                    angular.forEach(response.data.items, function (child) {
                        videos.push({id: child.id, snippet: child.snippet});
                    });

                    return {
                        prevPageToken: response.data.prevPageToken,
                        nextPageToken: response.data.nextPageToken,
                        videos: videos
                    };
                }, function (response) {
                    var error = "Error in retrieving videos\n"
                        + "Error Code : " + response.status + "\n"
                        + "Parameter : " + response.data.error.errors[0].location + "\n"
                        + "Message : " + response.data.error.errors[0].message;
                    console.log(error);
                    return -1;
                });
            },

            /**
             * Retrieves a specific video using $http.get() request.
             * @param id
             * @success YouTube video Snippet (basic video details) and statistics
             *      (number of comments, likes, dislikes and view count) of video is extracted
             *      from the response and returned as JSON object.
             * @error response will contain necessary data explaining the error that has occurred.
             *      Error code, parameter that caused the error and error message will be printed using the console.log()
             *      method (This is only for development purposes). Returns -1 to indicates that get() request failed.
             */
            getEpisode: function (id) {
                videoParams.id = id;

                return $http.get('https://www.googleapis.com/youtube/v3/videos', {params:videoParams}).then(function (response) {
                    return {
                        snippet: response.data.items[0].snippet,
                        statistics: response.data.items[0].statistics
                    };
                }, function (response) {
                    var error = "Error in retrieving video\n"
                        + "Error Code : " + response.status + "\n"
                        + "Parameter : " + response.data.error.errors[0].location + "\n"
                        + "Message : " + response.data.error.errors[0].message;
                    console.log(error);
                    return -1;
                });
            }
        };
    })

    /**
     * Retrieves a thread of comments for a specific video.
     * Uses parameters defined in YouTube API documentation to get results matching the request.
     * Uses $http service to request the API service from YouTube.
     */
    .factory('YouTubeCommentFactory', function ($http) {
        var commentParams = {
            part: 'snippet',
            maxResults: '10',
            textFormat: 'plainText',
            videoId: '',
            key: 'AIzaSyARg-AM1ym2C11SkxOLHBAuulZbesMO2Ic'
        };

        return {
            /**
             * Retrieves the comment thread of a video
             * @param id
             * @returns {*}
             * @success YouTube comment Snippet (name, comment and other data) of video is extracted
             *      from the response and returned as JSON object.
             * @error response will contain necessary data explaining the error that has occurred.
             *      Error code, and error message will be printed using the console.log() method
             *      (This is only for development purposes). Returns -1 to indicates that get() request failed.
             */
            getCommentThread: function (id) {
                commentParams.videoId = id;
                var comments = [];

                return $http.get('https://www.googleapis.com/youtube/v3/commentThreads', {params:commentParams}).then(function (response) {
                    angular.forEach(response.data.items, function (child) {
                        comments.push(child.snippet.topLevelComment.snippet);
                    });
                    
                    return comments;
                }, function (response) {
                    var error = "Error in retrieving comments\n"
                        + "Error Code : " + response.status + "\n"
                        + "Message : " + response.data.error.errors[0].message;
                    console.log(error);
                    return -1;

                });
            }
        }
    })

    /**
     * Contains methods to handle tasks related to posts.
     * Uses angularFire $firebaseArray and $firebaseObject to do DB related tasks
     */
    .factory('PostFactory', function ($firebaseArray, $firebaseObject, $localStorage, $rootScope) {
        var ref = new Firebase("https://colombo.firebaseio.com"); //base reference for firebase connection
        var auth = ref.getAuth();

        var userId, username, profileImage, provider = auth.provider, group;

        //get userId, username and profileImage based on current authentication
        if (provider == "facebook"){
            userId = auth.facebook.id;
            username = auth.facebook.displayName;
            profileImage = auth.facebook.profileImageURL;
        } else if (provider == "google") {
            userId = auth.google.id;
            username = auth.google.displayName;
            profileImage = auth.google.profileImageURL;
        } else if (provider == "password") {
            userId = auth.uid;
            username = $localStorage.name;
            profileImage = "data:image/jpeg;base64," + $localStorage.imagee;
        }

        return {
            /**
             * Pushes a post to firebase depending on availability of groupId.
             * If group ID is null it is pushed to public posts else group posts.
             * @param groupId
             * @param text
             * @param imageArray
             */
            publish: function (groupId, text, imageArray) {
                //construct post data as json
                var post = {
                    postId: -new Date().getTime(),
                    userId: userId,
                    username: username,
                    profileImage: profileImage,
                    dateTime: new Date().getTime(),
                    post: text
                };

                //check groupId value and publish post to group or public
                if (groupId != null) {
                    var groupPost = $firebaseArray(ref.child("groupPosts/" + groupId));

                    groupPost.$add(post).then(function (ref) {
                        var length = imageArray.length;

                        if (length > 0) {
                            var count = 0;
                            for (var i = 0; i < length; i++) {
                                window.plugins.Base64.encodeFile(imageArray[i].src, function (data) {
                                    var image = {};
                                    image[count++] = { src: data };
                                    ref.child("images").update(image);
                                })
                            }
                        }
                    })
                } else {
                    var publicPost = $firebaseArray(ref.child("publicPosts"));

                    publicPost.$add(post).then(function (ref) {
                        var length = imageArray.length;

                        if (length > 0) {
                            var count = 0;
                            for (var i = 0; i < length; i++) {
                                window.plugins.Base64.encodeFile(imageArray[i].src, function (data) {
                                    var image = {};
                                    image[count++] = { src: data };
                                    ref.child("images").update(image);
                                })
                            }
                        }
                    })
                }
            },

            /**
             * Checks for availability of groupId and pushes comment for a post.
             * If group ID is null it is pushed to a public post else group post.
             * @param groupId
             * @param postId
             * @param text
             */
            comment: function (groupId, postId, text) {
                var entry;

                //construct comment data as json
                var comment = {
                    commentId: -new Date().getTime(),
                    userId: userId,
                    username: username,
                    profileImage: profileImage,
                    dateTime: new Date().getTime(),
                    comment: text
                };

                //checks groupId value and create reference for group post or public post
                if (groupId != null) {
                    entry = $firebaseArray(ref.child("groupPosts/" + groupId + "/" + postId + "/comments"));
                } else {
                    entry = $firebaseArray(ref.child("publicPosts/" + postId + "/comments"));
                }

                //adds comment to relevant post in firebase
                entry.$add(comment);
                //update number of comments of post in firebase
                entry.$ref().once('value', function (snapshot) {
                    entry.$ref().parent().update({ commentCount: snapshot.numChildren() });
                })
            },

            /**
             * Returns posts of specific group in order of newest to old.
             * @param groupId
             */
            getPostsPerGroup: function (groupId) {
                group = groupId;
                return $firebaseArray(ref.child("groupPosts/" + group).orderByChild("postId"));
            },

            /**
             * Returns public posts in order of newest to old.
             */
            getNewsfeed: function () {
                group = null;
                return $firebaseArray(ref.child("publicPosts").orderByChild("postId"));
            },

            /**
             * Gets a list of user IDs followed by currently logged in user.
             * Returns the list of IDs as an array.
             */
            getFollowing: function () {
                //firebase reference for current profile
                var curProfile = $firebaseObject(ref.child("profile").orderByChild("id").equalTo($localStorage.idd));
                //firebase reference for all profiles
                var allProfiles = $firebaseObject(ref.child("profile"));

                return curProfile.$loaded().then(function (data) {
                    var key = Object.keys(data)[3];
                    var following = data[key].following;
                    var followingId = [];

                    return allProfiles.$loaded().then(function (profiles) {
                        angular.forEach(following, function (follow) {
                            angular.forEach(profiles, function (value, key) {
                                if (key == follow.following_id)
                                    followingId.push(value.id);
                            })
                        });

                        return followingId;
                    })
                });
            },

            /**
             * Checks availability of groupId and returns a group post or public post that matches the postId.
             * Returns post from firebase
             * @param groupId
             * @param postId
             */
            getPost: function (groupId, postId) {
                if (groupId != null) {
                    return $firebaseArray(ref.child("groupPosts/" + groupId).orderByKey().equalTo(postId));
                } else {
                    return $firebaseArray(ref.child("publicPosts/").orderByKey().equalTo(postId));
                }
            },

            /**
             * Shares a post from group or public to public posts.
             * @param post
             * @param origin
             */
            sharePost: function (post, origin) {
                //firebase reference for public posts
                var sharedPost = $firebaseArray(ref.child("publicPosts"));

                //construct share data as json
                var share = {
                    postId: -new Date().getTime(),
                    userId: userId,
                    username: username,
                    profileImage: profileImage,
                    dateTime: new Date().getTime(),
                    post: post.post,
                    originalUser: post.username,
                    originalDate: post.dateTime,
                    origin: origin
                };

                //checks if post has images
                if (post.hasOwnProperty("images"))
                    share["images"] = post.images;

                //push data to firebase
                sharedPost.$add(share);
            },

            /**
             * Checks if a post is liked and returns true or false.
             * Checks availability of groupId to determine where to check
             * @param groupId
             * @param postId
             */
            isLiked: function (groupId, postId) {
                if (group != null) {
                    return $firebaseObject(ref.child("groupPosts/" + groupId).orderByKey().equalTo(postId)).$loaded().then(function (data) {
                        var liked = false;

                        if (data[postId].hasOwnProperty("likes")) {
                            angular.forEach(data[postId].likes, function (value) {
                                if (value == userId)
                                    liked = true;
                            });
                        }

                        return liked;
                    });
                } else {
                    return $firebaseObject(ref.child("publicPosts/").orderByKey().equalTo(postId)).$loaded().then(function (data) {
                        var liked = false;

                        if (data[postId].hasOwnProperty("likes")) {
                            angular.forEach(data[postId].likes, function (value) {
                                if (value == userId)
                                    liked = true;
                            });
                        }

                        return liked;
                    });
                }
            },

            /**
             * Checks availability of groupId to add like to relevant post.
             * @param groupID
             * @param postId
             */
            like: function (groupID, postId) {
                var entry;

                if (group != null) {
                    entry = ref.child("groupPosts/" + groupID + "/" + postId + "/likes");
                } else {
                    entry = ref.child("publicPosts/" + postId + "/likes");
                }

                //push user id to likes of post
                entry.push(userId);
                //update like count
                entry.once('value', function (snapshot) {
                    entry.parent().update({ likeCount: snapshot.numChildren() });
                });
            },

            /**
             * Checks availability of groupId and deletes entry for like
             * @param groupID
             * @param postId
             */
            unlike: function (groupID, postId) {
                var entry;

                if (group != null) {
                    entry = $firebaseObject(ref.child("groupPosts/" + groupID + "/" + postId + "/likes"));
                } else {
                    entry = $firebaseObject(ref.child("publicPosts/" + postId + "/likes"));
                }

                angular.forEach(entry, function (value, key) {
                    if (value == userId)
                        delete entry[key];
                });

                //remove user id from likes of posts
                entry.$save();
                //update like count
                entry.$ref().once('value', function (snapshot) {
                    entry.$ref().parent().update({ likeCount: snapshot.numChildren() });
                });
            }
        };
    })

    /**
     * Contains methods to handle image from camera or library.
     * Injects $cordovaCamera and $cordovaImagePicker to get images.
     */
    .factory('CameraFactory', function ($q, $cordovaCamera, $cordovaImagePicker) {
        return {
            /**
             * Gets image from camera using cordova camera plugin.
             * Requires a set of options to be defined to capture image.
             * Returns a promise of data captured.
             */
            getPictureFromCamera: function () {
                var q = $q.defer();

                var options = {
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    encodingType: Camera.EncodingType.JPEG,
                    quality: 100,
                    targetWidth: 1280,
                    targetHeight: 1280,
                    allowEdit: false,
                    correctOrientation:true,
                    saveToPhotoAlbum: true
                };

                $cordovaCamera.getPicture(options).then(
                    function (data) {
                        q.resolve(data);
                    }, function (error) {
                        q.reject(error);
                    }
                );

                return q.promise;
            },

            /**
             * Gets images from library using cordova image picker plugin.
             * Requires a set of options to be defined to get selected images.
             * Returns a promis of data captured.
             * @returns {*}
             */
            getPictureFromLibrary: function () {
                var q = $q.defer();

                var options = {
                    maximumImagesCount: 50,
                    width: 1280,
                    height: 1280,
                    quality: 100
                };

                $cordovaImagePicker.getPictures(options).then(
                    function (data) {
                        q.resolve(data);
                    }, function (error) {
                        q.reject(error);
                    });

                return q.promise;
            }
        };
    });