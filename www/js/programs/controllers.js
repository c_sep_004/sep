angular.module('programs.controllers', ['ngSanitize'])

    /**
     * Stores all the programs to be accessed by the view.
     * Requires $scope for 2-way data binding and ProgramsFactory to get programs.
     */
    .controller("ProgramsCtrl", function ($scope, $rootScope, ProgramsFactory) {
        /* Google Analytics: Track view */
        if(typeof analytics !== 'undefined') { analytics.trackView("Programs view"); }

        $rootScope.notify("Loading Programs...");
        $scope.programs = ProgramsFactory.getAllPrograms();
    })

    /**
     * Contains methods to load and browse a list of videos in the program page.
     * Requires $scope for 2-way data biding, $stateParams to capture data over URL, $sce to set resource as trusted,
     * $ionicLoading to show delay, ProgramsFactory and ProgramFactory to get video results.
     * Contains 5 methods to implement functionality of program page.
     */
    .controller("ProgramEpisodesCtrl", function ($scope, $rootScope, $stateParams, $sce, $ionicLoading, $ionicScrollDelegate, ProgramsFactory, ProgramFactory) {
        $scope.program = ProgramsFactory.getProgram($stateParams.programId); // retrieve program relevant to current page
        $scope.select = {}; // initialize dropdowns in program view
        $scope.select.sortBy = "date"; // set initial option for sortby dropdown
        $scope.select.videosPerPage = "10"; // set initial option for videos per page

        var pageToken = ''; // initial page token to load first page

        /**
         * Retrieves the necessary video data to display list of episodes in program view.
         * @param pageToken
         * Extracts page tokens and videos and stores into tokens and episodes array.
         * Data is accessed using 2-way binding within program view.
         */
        $scope.displayEpisodeList = function (pageToken) {
            $ionicScrollDelegate.scrollTop();
            $rootScope.notify("Loading Episodes...");
            $scope.episodes = []; // initialize episodes
            $scope.tokens = {
                prevPageToken: '',
                nextPageToken: ''
            }; // initialize page tokens

            var maxResults = $scope.select.videosPerPage, // assign current videos per page
                order = $scope.select.sortBy, // assign sorting order
                q = $scope.program.title; // assign program title

            ProgramFactory.getEpisodeList(maxResults, order, q, pageToken).then(function (response) {
                // check if episode list retrieval was successful
                if (response != "-1") {
                    // push previous and next page tokens into tokens array
                    if (angular.isUndefined(response.prevPageToken)) {
                        $scope.tokens.prevPageToken = null;
                    } else {
                        $scope.tokens.prevPageToken = response.prevPageToken;
                    }
                    $scope.tokens.nextPageToken = response.nextPageToken;

                    console.log($scope.tokens);

                    // iterate though each video JSON object, extract and push into episodes array
                    angular.forEach(response.videos, function (object) {
                        $scope.episodes.push({
                            videoId: object.id.videoId,
                            publishedAt: object.snippet.publishedAt,
                            title: object.snippet.title,
                            thumbnail: $sce.trustAsResourceUrl('https://i.ytimg.com/vi/' + object.id.videoId + '/hqdefault.jpg')
                        });
                    });
                }
            });
        };

        $scope.visible = false; // Set initial filter visibility to false

        /**
         * Toggle filter to show and hide.
         * Triggers displayEpisodeList() function if filter has been tapped to hide.
         */
        $scope.showFilter = function () {
            $scope.visible = $scope.visible !== true;

            if ($scope.visible == false) {
                $scope.displayEpisodeList(pageToken);
            }
        };

        $scope.displayEpisodeList(pageToken); // trigger function to load videos on page load
    })

    /**
     * Contains methods to load a video and its statistics including comments.
     * Requires $scope for 2-way data biding, $stateParams to capture data over URL, $sce to set resource as trusted,
     * ProgramFactory and YouTubeCommentFactory to get video results.
     * Contains 2 methods to implement functionality of episode page.
     */
    .controller("ProgramEpisodeCtrl", function ($scope, $stateParams, $sce, ProgramFactory, YouTubeCommentFactory) {
        $scope.videoId = $stateParams.videoId; // assign YouTube video ID
        $scope.episode = []; // initialize array to hold episode details

        /**
         * Retrieves the necessary video data to display in episode page
         * Extracts basic video information and statistics and stores into episode array.
         * Data is accessed using 2-way binding within episode view.
         */
        $scope.displayEpisode = function () {
            ProgramFactory.getEpisode($scope.videoId).then(function (response) {
                $scope.episode.push({
                    description: response.snippet.description,
                    publishedAt: response.snippet.publishedAt,
                    title: response.snippet.title,
                    url: $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + $scope.videoId),
                    viewCount: response.statistics.viewCount,
                    likeCount: response.statistics.likeCount,
                    dislikeCount: response.statistics.dislikeCount,
                    commentCount: response.statistics.commentCount
                });
            });
        };

        /**
         * Retrieves the main comment thread related to a video for display.
         * Retrieved data contains the necessary sections (author, message, posted date of comment).
         * Data is accessed using 2-way binding within episode view.
         */
        $scope.displayCommentThread = function () {
            $scope.comments = [];

            YouTubeCommentFactory.getCommentThread($scope.videoId).then(function (response) {
                if (response != "-1")
                    $scope.comments = response;
            });
        };

        $scope.displayEpisode();
        $scope.displayCommentThread();
    })

    /**
     * Loads posts related to a program.
     * Requires $scope for 2-way data binding, $stateParams to get groupId, $ionicModal to create modals for post view and publish,
     * $ionicScrollDelegate to control scroll of page, $ionicPopup for custom alert box, $rootScope to access global variables.
     * Requires PogramsFactory and PostFactory
     */
    .controller("ProgramGroupCtrl", function ($scope, $stateParams, $ionicModal, $ionicScrollDelegate, $ionicSlideBoxDelegate,
                                             $ionicPopup, $rootScope, ProgramsFactory, PostFactory) {
        var ref = new Firebase("https://colombo.firebaseio.com/");
        var auth = ref.getAuth();
        var provider = auth.provider;

        //set auth data from type of authentication
        if (provider == "facebook"){
            $scope.userId = auth.facebook.id;
        } else if (provider == "google") {
            $scope.userId = auth.google.id;
        } else if (provider == "password") {
            $scope.userId = auth.uid;
        }

        var groupId = $stateParams.programId;

        //bind posts data to access in view
        PostFactory.getPostsPerGroup(groupId).$loaded().then(function (data) {
            $rootScope.notify("Loading Group Posts...");
            $scope.posts = data;
        });

        $scope.program = ProgramsFactory.getProgram(groupId);

        //set liked to true or false
        ProgramsFactory.isLiked(groupId).then(function (data) {
            $scope.liked = data;
        });

        /* modal view */
        //create post view modal
        $ionicModal.fromTemplateUrl('templates/programs/postViewModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.postViewModal = modal;
        });

        //create post publish modal
        $ionicModal.fromTemplateUrl('templates/programs/postPublishModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.postPublishModal = modal;
        });

        //function to load modals
        $scope.loadModal = function (id) {
            if (id == 0) {
                $scope.postPublishModal.show();
            } else {
                PostFactory.getPost(groupId, id).$loaded().then(function (data) {
                    $scope.post = data;
                    PostFactory.isLiked(groupId, id).then(function (liked) {
                        $rootScope.postLiked = liked;
                    });
                });
                $scope.postViewModal.show();
            }
        };

        //function to dismiss modals
        $scope.dismissModal = function (id) {
            if (id == 0) {
                $scope.postPublishModal.hide();
            } else {
                $scope.post = {};
                $scope.postViewModal.hide();
            }
        };

        /* group sections and effects */
        //function to handle like and unlike from view
        $scope.like = function (program) {
            if ($scope.liked) {
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Confirmation',
                    template: 'Are you sure you wish to unlike ' + program + '?',
                    cancelText: 'No',
                    cancelType: 'button-positive',
                    okText: 'Yes',
                    okType: 'button-assertive'
                });

                confirmPopup.then(function (res) {
                    if (res) {
                        $scope.liked = false;
                        ProgramsFactory.unlike(groupId);
                    }
                });
            } else {
                $scope.liked = true;
                ProgramsFactory.like(groupId)
                
                // Track event for facebook login
                if (typeof analytics !== 'undefined') { analytics.trackEvent("Likes", "like", "Like"); }
            }
        };

        $scope.onUserDetailContentScroll = function () {
            var scrollDelegate = $ionicScrollDelegate.$getByHandle('userDetailContent');
            var scrollView = scrollDelegate.getScrollView();
            $scope.$broadcast('userDetailContent.scroll', scrollView);
        };
    })

    /**
     * Loads public posts.
     * Requires $scope for 2-way data binding, $ionicModal to create modals for post view and publish,
     * $rootScope to access global variables, $localStorage to access cached data.
     * Requires PostFactory
     */
    .controller('NewsfeedCtrl', function ($scope, $rootScope, $localStorage, $ionicModal, PostFactory) {
        //firebase reference
        var ref = new Firebase("https://colombo.firebaseio.com/");
        var auth = ref.getAuth();
        var provider = auth.provider;
        $scope.visible = false;

        //set auth data from type of authentication
        if (provider == "facebook"){
            $scope.userId = auth.facebook.id;
            $scope.username = auth.facebook.displayName;
            $scope.profileImage = auth.facebook.profileImageURL;
        } else if (provider == "google") {
            $scope.userId = auth.google.id;
            $scope.username = auth.google.displayName;
            $scope.profileImage = auth.google.profileImageURL;
        } else if (provider == "password") {
            $scope.userId = auth.uid;
            $scope.username = $localStorage.name;
            $scope.profileImage = "data:image/jpeg;base64," + $localStorage.imagee;
        }

        //globally accessible function to refresh newsfeed
        $rootScope.refreshNewsfeed = function () {
            PostFactory.getNewsfeed().$loaded().then(function (posts) {
                $scope.posts = [];

                PostFactory.getFollowing().then(function (following) {
                    following.push($scope.userId);

                    angular.forEach(posts, function (post) {
                        if (following.indexOf(post.userId) != -1) {
                            $scope.posts.push(post);
                        }
                    });
                });
            });
            $scope.$broadcast("scroll.refreshComplete");
        };

        //create post view modal
        $ionicModal.fromTemplateUrl('templates/programs/postViewModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.postViewModal = modal;
        });

        //create post publish modal
        $ionicModal.fromTemplateUrl('templates/programs/postPublishModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.postPublishModal = modal;
        });

        //function to load modals
        $scope.loadModal = function (id) {
            if (id == 0) {
                $scope.postPublishModal.show();
            } else {
                PostFactory.getPost(null, id).$loaded().then(function (data) {
                    $scope.post = data;

                    PostFactory.isLiked(null, data[0].$id).then(function (liked) {
                        $rootScope.postLiked = liked;
                    });
                });
                $scope.postViewModal.show();
            }
        };

        //function to dismiss modals
        $scope.dismissModal = function (id) {
            if (id == 0) {
                $scope.refreshNewsfeed();
                $scope.postPublishModal.hide();
            } else {
                $scope.post = {};
                $scope.postViewModal.hide();
            }
        };

        $rootScope.notify("Loading Newsfeed...");
        $scope.refreshNewsfeed();
    })

    /**
     * Loads all data related to a specific post.
     * Requires $scope for 2-way data binding, $stateParams to get groupId, $rootScope to access global variables.
     * Requires PostFactory
     */
    .controller('PostViewModalCtrl', function ($scope, $rootScope, $stateParams, $ionicScrollDelegate, $ionicPopup, PostFactory) {
        $scope.message = {};
        $scope.message.text = '';
        var groupId;

        //set group id if exists
        if (angular.isUndefined($stateParams.programId))
            groupId = null;
        else
            groupId = $stateParams.programId;

        //function to set post like or unliked status
        $scope.toggleLike = function (action, postId) {
            if (action == "like") {
                PostFactory.like(groupId, postId);
            }
            else if (action == "unlike") {
                PostFactory.unlike(groupId, postId);
            }

            PostFactory.isLiked(groupId, postId).then(function (res) {
                $rootScope.postLiked = res;
            });
        };

        //function to add persons name in comment as reply
        $scope.tagInComment = function (name) {
            $scope.message.text = "#" + name + " ";
        };

        //scroll to bottom after posting comment
        $scope.ajustarScroll = function () {
            $ionicScrollDelegate.resize();
            $ionicScrollDelegate.scrollBottom(true);
        };

        //function to post comment
        $scope.postComment = function (postId) {
            PostFactory.comment(groupId, postId, $scope.message.text);
            $scope.message.text = '';
            $ionicScrollDelegate.scrollBottom(true);

            // Track event for facebook login
            if (typeof analytics !== 'undefined') { analytics.trackEvent("Comments", "comment", "Comment"); }
        };

        //function to share post
        $scope.sharePost = function (post) {
            if (groupId == null) {
                PostFactory.sharePost(post, "Public");
            }
            else {
                PostFactory.sharePost(post, "Group");
            }

            $rootScope.notify("Post Shared!");
        };

        //function to delete post
        $scope.deletePost = function (postId) {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Confirmation',
                template: 'Are you sure you wish to delete this post?',
                cancelText: 'No',
                cancelType: 'button-positive',
                okText: 'Yes',
                okType: 'button-assertive'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    var ref;

                    if (groupId == null) {
                        ref = new Firebase("https://colombo.firebaseio.com/publicPosts/" + postId);
                    } else {
                        ref = new Firebase("https://colombo.firebaseio.com/groupPosts/" + groupId + "/" + postId);
                    }

                    ref.set(null);

                    if (groupId == null) {
                        $rootScope.refreshNewsfeed();
                    }
                }
            });
        };
    })

    /**
     * Controller to handle post publishing
     * Requires $scope for 2-way data binding, $stateParams to get groupId, $rootScope to access global variables.
     * Requires PostFactory and CameraFactory.
     */
    .controller('PostPublishModalCtrl', function ($scope, $stateParams, $rootScope, CameraFactory, PostFactory) {
        $scope.publish = {};
        $scope.publish.text = '';
        $scope.imageURLs = [];
        var groupId;

        //set group id if exists
        if (angular.isUndefined($stateParams.programId))
            groupId = null;
        else
            groupId = $stateParams.programId;

        //trigger functions only if device is available
        document.addEventListener("deviceready", function () {
            //function to get images and store in array
            $scope.getImages = function (source) {
                if (source == 'camera') {
                    CameraFactory.getPictureFromCamera().then(function (data) {
                        $scope.imageURLs.push({ src: data });
                    });
                } else if (source == 'library') {
                    CameraFactory.getPictureFromLibrary().then(function (data) {
                        var len = data.length;

                        for (var i = 0; i < len; i++) {
                            $scope.imageURLs.push({ src: data[i] });
                        }
                    });
                }
            };

            //function to clear images
            $scope.clearImages = function () {
                $scope.imageURLs = [];
            };
        }, false);

        //publish post and clear entries
        $scope.uploadToFirebase = function () {
            PostFactory.publish(groupId, $scope.publish.text, $scope.imageURLs);
            $scope.publish.text = '';
            $scope.imageURLs = [];
            $rootScope.notify("Post Published!");
        }
    })

    /**
     * Loads favourite programs.
     * Requires $scope for 2-way data binding, $rootScope to access global variables.
     * Requires ProgramsFactory
     */
    .controller("FavouriteCtrl", function ($scope, $rootScope, ProgramsFactory) {
        //global function to refresh favourite programs
        $rootScope.refreshFavourite = function () {
            ProgramsFactory.getAllPrograms().$loaded().then(function (programs) {
                $scope.programs = [];

                ProgramsFactory.getFavouriteProgram().then(function (favourites) {
                    angular.forEach(programs, function (program) {
                        if (favourites.indexOf(program.id) != -1)
                            $scope.programs.push(program);
                    });
                })
            });
            $scope.$broadcast("scroll.refreshComplete");
        };

        $rootScope.notify("Loading Favourites...");
        $rootScope.refreshFavourite();
    })

    /**
     * Handles location data to show nearby friends and get current location.
     * Requires $scope for 2-way data binding, $rootScope to access global variables, $localStorage to get cached data,
     * $firebaseObject to handle firebase data and $cordovaGeolocation to get location data.
     * Requires PostFactory
     */
    .controller("NearbyCtrl", function ($scope, $rootScope, $localStorage, $firebaseObject, $cordovaGeolocation, PostFactory) {
        //firebase reference
        var ref = new Firebase("https://colombo.firebaseio.com/");
        var locations = $firebaseObject(ref.child("locations"));
        var auth = ref.getAuth();
        var provider = auth.provider;

        //set auth data from type of authentication
        if (provider == "facebook"){
            $scope.userId = auth.facebook.id;
            $scope.username = auth.facebook.displayName;
            $scope.profileImage = auth.facebook.profileImageURL;
        } else if (provider == "google") {
            $scope.userId = auth.google.id;
            $scope.username = auth.google.displayName;
            $scope.profileImage = auth.google.profileImageURL;
        } else if (provider == "password") {
            $scope.userId = auth.uid;
            $scope.username = $localStorage.name;
            $scope.profileImage = "data:image/jpeg;base64," + $localStorage.imagee;
        }

        //set initial share location state to false
        $scope.setting = {
            shareLocation: false
        };
        $scope.nearby = [];

        //set location sharing to true if data exists in firebase
        locations.$loaded().then(function (data) {
            angular.forEach(data, function (value) {
                if (value.userId == $scope.userId) {
                    $scope.setting.shareLocation = true;
                    $scope.refreshLocations();
                }
            })
        });

        //function to get and resolve location to set data in firebase
        var setLocation = function () {
            var options = {
                maximumAge: 3000,
                timeout: 5000,
                enableHighAccuracy: true
            };

            $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
                $scope.coords = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                var location = {
                    location: {
                        lat: $scope.coords.lat,
                        lng: $scope.coords.lng
                    }
                };

                var geocoder = new google.maps.Geocoder;

                geocoder.geocode(location, function (results, status) {
                    if (status == "OK") {
                        $scope.currentLocation = results[0].formatted_address;

                        var userLocation = {
                            userId: $scope.userId,
                            username: $scope.username,
                            profileImage: $scope.profileImage,
                            location: $scope.currentLocation,
                            latitude: $scope.coords.lat,
                            longitude: $scope.coords.lng,
                            dateTime: new Date().getTime()
                        };

                        ref.child("locations").push(userLocation);
                        getNearbyFriends();
                        $rootScope.notify("Location Updated!");
                    } else {
                        $scope.setting.shareLocation = false;
                        $scope.currentLocation = "Unavailable";
                        $rootScope.notify("Update Failed. Please Try Again Later");
                    }
                });
            }, function (error) {
                $rootScope.notify("Enable location services to find nearby friends.");
                $scope.setting.shareLocation = false;
                console.log(error);
            });
        };

        //function to remove data if sharing location was disabled
        var unsetLocation = function () {
            locations.$loaded().then(function (data) {
                angular.forEach(data, function (value, key) {
                    if (value.userId == $scope.userId) {
                        delete locations[key];
                        locations.$save();
                    }
                })
            })
        };

        //function to get nearby friends that are sharing location
        var getNearbyFriends = function () {
            locations.$loaded().then(function (location) {
                PostFactory.getFollowing().then(function (follow) {
                    angular.forEach(location, function (l) {
                        angular.forEach(follow, function (f) {
                            if (l.userId == f) {
                                if ((($scope.coords.lat - 0.1) <= l.latitude) && (l.latitude <= ($scope.coords.lat + 0.1))) {
                                    if ((($scope.coords.lng - 0.1) <= l.longitude) && (l.longitude <= ($scope.coords.lng + 0.1))) {
                                        $scope.nearby.push(l);
                                    }
                                }
                            }
                        });
                    });
                });
            });
        };

        //function to reload location based on location sharing state
        $scope.refreshLocations = function () {
            $scope.nearby = [];
            if ($scope.setting.shareLocation == true) {
                unsetLocation();
                setLocation();
            } else if ($scope.setting.shareLocation == false) {
                unsetLocation();                
            }

            $scope.$broadcast("scroll.refreshComplete");
        };
    })

    /**
     * Controller to store latitude and longitude.
     * Requires $scope for 2-way data binding and $stateParams to access location data
     */
    .controller("MapCtrl", function ($scope, $stateParams) {
        $scope.location = {
            latitude: $stateParams.latitude,
            longitude: $stateParams.longitude
        }
    });