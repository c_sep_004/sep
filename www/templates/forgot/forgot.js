/** This module will handle the password reset functions */

'Use Strict';
angular.module('app').controller('forgotController', function ($scope, $state,$cordovaOauth, $localStorage, $location,$http,$ionicPopup, $firebaseObject, Auth, FURL, Utils) 
{ 
    /* Retrieve firebase URL path*/
    var ref = new Firebase(FURL);
  
    /** Triggered when user hits forgot password button.
      * Function will call the resetpasssword method in auth.js
      * User will be notified once the password reset link has been sent.
      * User will be redirected to login page.
      */
    $scope.resetpassword = function(user) 
    {
      /* Check whether the user is a defined user */
      if(angular.isDefined(user))
      {
       /* Triggers to send the password reset link */
       Auth.resetpassword(user).then(function()
        {
           /* Redirect user to login page */
           $location.path('/login');
        }, function(err) 
            {
             console.error("Error: ", err);
            });
       }
    };
});
