'Use Strict';
angular.module('app').controller('menuController', function ($scope, $state,$cordovaOauth, $localStorage, $location,$http,$ionicPopup, $firebaseObject,$window, $ionicHistory, Auth, FURL, Utils) 
{
  var ref = new Firebase(FURL);

  $scope.logOut = function () 
  {
    Auth.logout();
    $window.localStorage.clear();
    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();
    $location.path("/login");
  }

  $scope.results = $localStorage.details;
});