/* This file handles all the functions related to login */

'Use Strict';
angular.module('app').controller('loginController', function ($scope, $state,$cordovaOauth, $localStorage, $location,$http,$ionicPopup, $timeout, $firebaseObject, $firebaseArray, $rootScope , Auth, FURL, Utils,$cordovaInAppBrowser) 
{
  var ref = new Firebase(FURL);
  var userkey = "";

  // Track login view
  if(typeof analytics !== 'undefined') { analytics.trackView("Login"); }

  /* Check connection before proceeding */
 /* var connectedRef = new Firebase(FURL+"/.info/connected");
  connectedRef.on("value", function(snap) {
    if (snap.val() === true) {
      connectingPopup.close();
      connectedPopup = $ionicPopup.show({
        title: 'Connected!'
      });
      $timeout(function() {
        connectedPopup.close();
      }, 1000);
    } else {
      connectingPopup = $ionicPopup.show({
        title: 'Connecting...'
      });
    }
  });*/

 /** 
   * Triggers when user hits sign in button.
   * check whether the user is a valid user.
   * if the credintials entered is not valid display error messages.
   */
  $scope.signIn = function (user) 
  {
    /* Determines whether the user is defined */
    if(angular.isDefined(user))
    {
     /* Call utils factory to display loding icon */
     Utils.show();
        
     /* Call the login function to do the login */
     Auth.login(user).then(function(authData)
        {
          /* If the login is successful take the user details to use inside the application */
          ref.child('profile').orderByChild("id").equalTo(authData.uid).on("child_added", function(snapshot) 
          {
          // Track event for facebook login
          if(typeof analytics !== 'undefined') { analytics.trackEvent("Login", "firebase_login", "Firebase Login"); }

           userkey = snapshot.key();
        
           /* Create a reference to the user profile */
           var obj = $firebaseObject(ref.child('profile').child(userkey));

           /* Assign all user details to local variables */
           obj.$loaded().then(function(data) 
             {
                console.log(obj);
                $localStorage.details = obj;
                $localStorage.email = obj.email;
                $localStorage.userkey = userkey;
                $localStorage.name = obj.username;
                $localStorage.country = obj.country;
                $localStorage.registered_date = obj.registered_in;
                $localStorage.idd = obj.id;
                $localStorage.imagee = obj.image;
                $localStorage.reg_type = obj.reg_type;
                /* Call util factory to hide the loading icon */
                Utils.hide();
                /* Make "Account" tab visible to the user since this is a who logs through application login */
                $rootScope.hideTabs = false;
                $rootScope.hidefbTabs = true;
                /* Redirect user to home page */
                $state.go('app.home');
    
            }).catch(function(error) /* If there is a error in loading details */
                {
                    console.error("Error:", error);
                });
           });

         }, function(err) 
            {
                /* Hide loading icon */
                Utils.hide();
                /* Call ustils factory to display error messages */
                Utils.errMessage(err);
            });
    }
  }
    
    
/** Triggered when login through facebook. */
    $scope.fbLogin = function() 
    {
        /* Call utils factory to display loding icon */
        Utils.show();
        /* Connect to facebook through firebase */
        ref.authWithOAuthPopup("facebook", function(error, authData) 
        {
          Utils.show();                        
          if (error)
           {
             Utils.hide();
             console.log("Login Failed!", error);
           }
          else
           {
              // Track event for facebook login
              if(typeof analytics !== 'undefined') { analytics.trackEvent("Login", "facebook_login", "Facebook Login"); }

               var fbref = new Firebase("https://colombo.firebaseio.com/profile");
               fbref.orderByChild("id").equalTo(authData.facebook.id).once("value", function(response) {
              
               $scope.fbid = response.val();                           
                
               
               var skey = "no_record";
                   if($scope.fbid)
                   {
                      skey = "have_record";
                   }           
               
               if(skey == "have_record")
                   {
                       /* Redirect user to home page */
                        $state.go('app.home');
                       
                       /* Storing user details in local variables */
                        $localStorage.name = authData.facebook.displayName;
                        console.log(authData);             
                        /* Hide "Account" tab in side bar */
                      //  $rootScope.hideTabs = true;
                    //   $rootScope.hidefbTabs = false;
             
                      angular.forEach($scope.fbid,function(value,index){
                        $rootScope.username = value.username;
                        $rootScope.age = value.Age;
                        $rootScope.image = value.image;
                        $rootScope.gender = value.Gender;
                        $rootScope.unique_id = value.unique_id;
                        $localStorage.details = value;
                        $localStorage.idd = value.id;
                        $localStorage.userkey = value.unique_id;
                        $localStorage.name = value.username;
                        $localStorage.imagee = value.image;
                        $localStorage.reg_type = value.reg_type;
                        
                        $rootScope.hideTabs = true;
                        $rootScope.hidefbTabs = false;
                        })
                      
                   } 
                else
                   {
                       
                     var profile = {
				            id: authData.facebook.id,
                            email: "none",
                            username: authData.facebook.displayName,
				            registered_in: Date(),
                            country: "none",
                            Gender: authData.facebook.cachedUserProfile.gender,
                            Age: authData.facebook.cachedUserProfile.age_range.min,
                            Contact_no: "none",
                            image: authData.facebook.profileImageURL,
                            unique_id:"",
                            reg_type:"facebook"
                        };
               
                        /* URL path reference to user profile */
                        var profileRef = $firebaseArray(ref.child('profile'));
    
                        profileRef.$add(profile).then(function(ref) 
                        {
			             var id = ref.key();         
                         var unique = profileRef.$getRecord(id);
                         unique.unique_id = id;
                         profileRef.$save(unique);
                         $localStorage.userkey = id;
			             });
               
                        /* Storing user details in local variables */
                        $localStorage.name = authData.facebook.displayName;
                        console.log(authData);             
                        /* Hide "Account" tab in side bar */
                       // $rootScope.hideTabs = true;
             
                        $rootScope.username = authData.facebook.displayName;
                        $rootScope.age = authData.facebook.cachedUserProfile.age_range.min;
                        $rootScope.image = authData.facebook.profileImageURL;
                        $rootScope.gender = authData.facebook.cachedUserProfile.genderr;
                        $rootScope.fb_id = authData.facebook.id;
                        $localStorage.idd = authData.facebook.id;
                       // $localStorage.userkey = authData.facebook.id;
                        $localStorage.name = authData.facebook.displayName;
                        $localStorage.imagee = authData.facebook.profileImageURL;
                        $localStorage.reg_type = "facebook";
                        
                        
                        $rootScope.hideTabs = true;
                        $rootScope.hidefbTabs = false;
                       
                        /* Redirect user to home page */
                        $state.go('app.home');
                   }
               })
           }
        
        });
        Utils.hide();
    };
    
/** Triggered when login through google. */
    $scope.googleLogin = function() 
    {
        Utils.show();
        /* Connect to facebook through firebase */
      
        // Track event for google login
        if(typeof analytics !== 'undefined') { analytics.trackEvent("Login", "google_login", "Google Login"); }

       ref.authWithOAuthPopup("google", function(error, authData) {
        if (error) {
          Utils.hide();
          console.log("Login Failed!", error);
        } else {
          console.log("Authenticated successfully with payload google:", authData);
           
            
            var fbref = new Firebase("https://colombo.firebaseio.com/profile");
               fbref.orderByChild("id").equalTo(authData.google.id).once("value", function(response) {
              
               $scope.fbid = response.val();                           
                
               
               var skey = "no_record";
                   if($scope.fbid)
                   {
                      skey = "have_record";
                   }           
               
               if(skey == "have_record")
                   {
                       /* Storing user details in local variables */
                        $localStorage.name = authData.google.displayName;
                        console.log(authData);             
                        /* Hide "Account" tab in side bar */
                      //  $rootScope.hideTabs = true;
                    //   $rootScope.hidefbTabs = false;
             
                      angular.forEach($scope.fbid,function(value,index){
                        $rootScope.username = value.username;
                        $rootScope.image = value.image;
                        $rootScope.gender = value.Gender;
                        $rootScope.unique_id = value.unique_id;
                        $localStorage.details = value;
                        $localStorage.idd = value.id;
                        $localStorage.userkey = value.unique_id;
                        $localStorage.name = value.username;
                        $localStorage.imagee = value.image;
                        $localStorage.gender = value.Gender;
                        $localStorage.unique_id = value.unique_id;
                        $localStorage.reg_type = value.reg_type;
                        
                        $rootScope.hideTabs = true;
                        $rootScope.hidefbTabs = false;
                        })
                      
                        /* Redirect user to home page */
                        $state.go('app.home');
                   } 
                else
                   {
                       
                     var profile = {
				            id: authData.google.id,
                            email: "none",
                            username: authData.google.displayName,
				            registered_in: Date(),
                            country: "none",
                            Gender: "none",
                            Age: "none",
                            Contact_no: "none",
                            image: authData.google.profileImageURL,
                            unique_id:"",
                            reg_type:"google"
                        };
               
                        /* URL path reference to user profile */
                        var profileRef = $firebaseArray(ref.child('profile'));
    
                        profileRef.$add(profile).then(function(ref) 
                        {
			             var id = ref.key();         
                         var unique = profileRef.$getRecord(id);
                         unique.unique_id = id;
                         profileRef.$save(unique);
                         $localStorage.userkey = id;
			             });
               
                        /* Storing user details in local variables */
                        $localStorage.name = authData.google.displayName;
                        console.log(authData);             
                        /* Hide "Account" tab in side bar */
                       // $rootScope.hideTabs = true;
             
                        $rootScope.username = authData.google.displayName;
                        $rootScope.image = authData.google.profileImageURL;
                        $rootScope.google_id = authData.google.id;
                        $localStorage.idd = authData.google.id;
                        $localStorage.name = authData.google.displayName;
                        $localStorage.imagee = authData.google.profileImageURL;
                        $localStorage.gender = value.Gender;
                        $localStorage.unique_id = authData.google.idd;
                        $localStorage.reg_type = value.reg_type;
                        
                        
                        $rootScope.hideTabs = true;
                        $rootScope.hidefbTabs = false;
                       
                        /* Redirect user to home page */
                        $state.go('app.home');
                   }
               })
            
         }
       });
    };
    Utils.hide();
});
