/** This module controlls all the user profile editing functions. 
  * edit generel information will be handled.
  *edit private information will be handled.
  */

'Use Strict';
angular.module('app').controller('editprofileController', function ($scope, $state,$cordovaOauth, $localStorage, $location,$http,$ionicPopup, $firebaseObject, $firebaseAuth,$firebaseArray, $cordovaImagePicker, Auth, FURL, Utils,$cordovaCamera, $ionicActionSheet) 
{
  /* Google Analytics: Track view */
  if(typeof analytics !== 'undefined') { analytics.trackView("Account view"); }

  /* Retrieve firebase URL */
  var ref = new Firebase(FURL);  
  var auth = $firebaseAuth(ref);
    
  /*  URL for user account records */
  var bit = new Firebase("https://colombo.firebaseio.com/profile"); 
  var profiles = $firebaseArray(bit);
  
  /** Triggered when user hits sign out tab in side bar
    * Function will clear cache for the session and redirect user to login page 
    */
  $scope.logOut = function () 
  {
      Auth.logout();
      $window.localStorage.clear(); 
      $ionicHistory.clearCache(); 
      $ionicHistory.clearHistory();
      $location.path("/login"); 
  }
  
  /* Retrieve logged in users details */
  $scope.results = $localStorage.details;
  $scope.editbtn = false;
  $scope.saveeditbtn = true;
  $scope.disable = true;
    
  /**Triggered when user hits edit button in accout page to edit user info.
    *function will enable the input for user to edit user information.
    *and also function will hide edit button and display save button
    */
   $scope.allowtoedit = function () 
   {
        $scope.editbtn = true;
        $scope.saveeditbtn = false;
        $scope.disable = false;
   } 
  
  /** Triggered when user hits edit button in account page to edit general information
    * Function will retrieve new information user entered and store in the database
    * User will be notified once the udpating is completed
    */
  $scope.edit = function (result) 
  {
      //Utils.show();
      /* Retrieve logged in users` details */
      $scope.iid = $localStorage.userkey;
      $scope.registered_dates = $localStorage.registered_date;
      $scope.uid = $localStorage.idd;
      $scope.emails = $localStorage.email;
     
      /* Update logged in users details */
        var item = profiles.$getRecord($scope.iid);
        item.username = result.username;
        item.country = result.country;
        item.Age = result.Age;
        item.Gender = result.Gender;
        item.Contact_no = result.Contact_no;
      
        $localStorage.name = result.username;
        $localStorage.country = result.country;
        $localStorage.Age = result.Age;
        $localStorage.Gender = result.gender;
        profiles.$save(item).then(function(ref) 
        {
            Utils.hide();
            /* Call utils factory to notify user that profile has been updated successfully */
            Utils.alertshow("Successfully","Profile info updated successfully");
        }, function(error) 
            {
                Utils.hide();
                console.log("Error:", error);
            });
        $scope.editbtn = false;
        $scope.saveeditbtn = true;
        $scope.disable = true;
      
        Utils.hide();

  }
  
   /** Triggered when user hits done button in password reset page
     * Function will change the passowrd of the user by using firebase resetpassword method
     * user will be notified once the updation is done and will be redirected to login page
     */
   $scope.changepasswords = function (user) 
   {   
       Utils.show();
        $scope.emails = $localStorage.email;
        /* Change password of the user with firebase change password method */
        auth.$changePassword({email: $scope.emails, oldPassword: user.oldPass, newPassword: user.newPass}).then(function() 
        {
            Utils.hide();
           /* Notify user if the passowrd is seccessfully changed */
           Utils.alertshow("Successfully","Password changed successfully");
           /* Logout user and redirect to login page */
           Auth.logout();
           $location.path("/login");
        }).catch(function(error) 
            {  
                Utils.hide();
               /* Notify user if the password is invalid */
               Utils.alertshow("Error","Current passowrd you entered is incorrect!");
            });
    };
    
    /** Triggered when user hits done button in change email page.
      * Function will change the email of the user by using firebase change email method
      * User will be notified once the updation is done and redirect user to the login page
      */
    $scope.changeemail = function (user) 
    {  
      Utils.show();
      $scope.iid = $localStorage.userkey;
      $scope.registered_dates = $localStorage.registered_date;
      $scope.uid = $localStorage.idd;
      $scope.emails = $localStorage.email;
    
       /* Change email of the user with firebase change email method */
       auth.$changeEmail(
           {
            oldEmail: user.oldemail,
            newEmail: user.newemail,
            password: user.password
           }).then(function() 
             {
                /* New user details to update "profile" records in database */
                $scope.iid = $localStorage.userkey;
                $scope.registered_dates = $localStorage.registered_date;
                $scope.uid = $localStorage.idd;
                $scope.emails = $localStorage.email;
                $scope.country_name = $localStorage.country;
                $scope.username = $localStorage.name;    
           
                /* Update profile records of the user */
                var obj = $firebaseObject(ref.child('profile').child($scope.iid));
                obj.username = $scope.username;
                obj.email = user.newemail;
                obj.registered_in= $scope.registered_dates;
                obj.id= $scope.uid;
                obj.country = $scope.country_name;
                obj.$save().then(function(ref) 
                            {
                                Utils.hide();
                                ref.key() === obj.$id; // true
                                /* Notify user that the password changed successfully and redirect to login page */
                                Utils.alertshow("Successfully","Email changed successfully");
                                Auth.logout();
                                $location.path("/login");
                            }, function(error) 
                                 {
                                    Utils.hide();
                                    console.log("Error:", error);
                                 });     
  
             }).catch(function(error) 
                {
                    Utils.hide();
                    /* Display error messages if there is any error */
                    Utils.errMessage(error);
                });     
      };    
    
    
    $scope.countries = [ 
    {name: $localStorage.country},
    {name: 'Afghanistan', code: 'AF'},
    {name: 'Åland Islands', code: 'AX'},
    {name: 'Albania', code: 'AL'},
    {name: 'Algeria', code: 'DZ'},
    {name: 'American Samoa', code: 'AS'},
    {name: 'Andorra', code: 'AD'},
    {name: 'Angola', code: 'AO'},
    {name: 'Anguilla', code: 'AI'},
    {name: 'Antarctica', code: 'AQ'},
    {name: 'Antigua and Barbuda', code: 'AG'},
    {name: 'Argentina', code: 'AR'},
    {name: 'Armenia', code: 'AM'},
    {name: 'Aruba', code: 'AW'},
    {name: 'Australia', code: 'AU'},
    {name: 'Austria', code: 'AT'},
    {name: 'Azerbaijan', code: 'AZ'},
    {name: 'Bahamas', code: 'BS'},
    {name: 'Bahrain', code: 'BH'},
    {name: 'Bangladesh', code: 'BD'},
    {name: 'Barbados', code: 'BB'},
    {name: 'Belarus', code: 'BY'},
    {name: 'Belgium', code: 'BE'},
    {name: 'Belize', code: 'BZ'},
    {name: 'Benin', code: 'BJ'},
    {name: 'Bermuda', code: 'BM'},
    {name: 'Bhutan', code: 'BT'},
    {name: 'Bolivia', code: 'BO'},
    {name: 'Bosnia and Herzegovina', code: 'BA'},
    {name: 'Botswana', code: 'BW'},
    {name: 'Bouvet Island', code: 'BV'},
    {name: 'Brazil', code: 'BR'},
    {name: 'British Indian Ocean Territory', code: 'IO'},
    {name: 'Brunei Darussalam', code: 'BN'},
    {name: 'Bulgaria', code: 'BG'},
    {name: 'Burkina Faso', code: 'BF'},
    {name: 'Burundi', code: 'BI'},
    {name: 'Cambodia', code: 'KH'},
    {name: 'Cameroon', code: 'CM'},
    {name: 'Canada', code: 'CA'},
    {name: 'Cape Verde', code: 'CV'},
    {name: 'Cayman Islands', code: 'KY'},
    {name: 'Central African Republic', code: 'CF'},
    {name: 'Chad', code: 'TD'},
    {name: 'Chile', code: 'CL'},
    {name: 'China', code: 'CN'},
    {name: 'Christmas Island', code: 'CX'},
    {name: 'Cocos (Keeling) Islands', code: 'CC'},
    {name: 'Colombia', code: 'CO'},
    {name: 'Comoros', code: 'KM'},
    {name: 'Congo', code: 'CG'},
    {name: 'Congo, The Democratic Republic of the', code: 'CD'},
    {name: 'Cook Islands', code: 'CK'},
    {name: 'Costa Rica', code: 'CR'},
    {name: 'Cote D\'Ivoire', code: 'CI'},
    {name: 'Croatia', code: 'HR'},
    {name: 'Cuba', code: 'CU'},
    {name: 'Cyprus', code: 'CY'},
    {name: 'Czech Republic', code: 'CZ'},
    {name: 'Denmark', code: 'DK'},
    {name: 'Djibouti', code: 'DJ'},
    {name: 'Dominica', code: 'DM'},
    {name: 'Dominican Republic', code: 'DO'},
    {name: 'Ecuador', code: 'EC'},
    {name: 'Egypt', code: 'EG'},
    {name: 'El Salvador', code: 'SV'},
    {name: 'Equatorial Guinea', code: 'GQ'},
    {name: 'Eritrea', code: 'ER'},
    {name: 'Estonia', code: 'EE'},
    {name: 'Ethiopia', code: 'ET'},
    {name: 'Falkland Islands (Malvinas)', code: 'FK'},
    {name: 'Faroe Islands', code: 'FO'},
    {name: 'Fiji', code: 'FJ'},
    {name: 'Finland', code: 'FI'},
    {name: 'France', code: 'FR'},
    {name: 'French Guiana', code: 'GF'},
    {name: 'French Polynesia', code: 'PF'},
    {name: 'French Southern Territories', code: 'TF'},
    {name: 'Gabon', code: 'GA'},
    {name: 'Gambia', code: 'GM'},
    {name: 'Georgia', code: 'GE'},
    {name: 'Germany', code: 'DE'},
    {name: 'Ghana', code: 'GH'},
    {name: 'Gibraltar', code: 'GI'},
    {name: 'Greece', code: 'GR'},
    {name: 'Greenland', code: 'GL'},
    {name: 'Grenada', code: 'GD'},
    {name: 'Guadeloupe', code: 'GP'},
    {name: 'Guam', code: 'GU'},
    {name: 'Guatemala', code: 'GT'},
    {name: 'Guernsey', code: 'GG'},
    {name: 'Guinea', code: 'GN'},
    {name: 'Guinea-Bissau', code: 'GW'},
    {name: 'Guyana', code: 'GY'},
    {name: 'Haiti', code: 'HT'},
    {name: 'Heard Island and Mcdonald Islands', code: 'HM'},
    {name: 'Holy See (Vatican City State)', code: 'VA'},
    {name: 'Honduras', code: 'HN'},
    {name: 'Hong Kong', code: 'HK'},
    {name: 'Hungary', code: 'HU'},
    {name: 'Iceland', code: 'IS'},
    {name: 'India', code: 'IN'},
    {name: 'Indonesia', code: 'ID'},
    {name: 'Iran, Islamic Republic Of', code: 'IR'},
    {name: 'Iraq', code: 'IQ'},
    {name: 'Ireland', code: 'IE'},
    {name: 'Isle of Man', code: 'IM'},
    {name: 'Israel', code: 'IL'},
    {name: 'Italy', code: 'IT'},
    {name: 'Jamaica', code: 'JM'},
    {name: 'Japan', code: 'JP'},
    {name: 'Jersey', code: 'JE'},
    {name: 'Jordan', code: 'JO'},
    {name: 'Kazakhstan', code: 'KZ'},
    {name: 'Kenya', code: 'KE'},
    {name: 'Kiribati', code: 'KI'},
    {name: 'Korea, Democratic People\'s Republic of', code: 'KP'},
    {name: 'Korea, Republic of', code: 'KR'},
    {name: 'Kuwait', code: 'KW'},
    {name: 'Kyrgyzstan', code: 'KG'},
    {name: 'Lao People\'s Democratic Republic', code: 'LA'},
    {name: 'Latvia', code: 'LV'},
    {name: 'Lebanon', code: 'LB'},
    {name: 'Lesotho', code: 'LS'},
    {name: 'Liberia', code: 'LR'},
    {name: 'Libyan Arab Jamahiriya', code: 'LY'},
    {name: 'Liechtenstein', code: 'LI'},
    {name: 'Lithuania', code: 'LT'},
    {name: 'Luxembourg', code: 'LU'},
    {name: 'Macao', code: 'MO'},
    {name: 'Macedonia, The Former Yugoslav Republic of', code: 'MK'},
    {name: 'Madagascar', code: 'MG'},
    {name: 'Malawi', code: 'MW'},
    {name: 'Malaysia', code: 'MY'},
    {name: 'Maldives', code: 'MV'},
    {name: 'Mali', code: 'ML'},
    {name: 'Malta', code: 'MT'},
    {name: 'Marshall Islands', code: 'MH'},
    {name: 'Martinique', code: 'MQ'},
    {name: 'Mauritania', code: 'MR'},
    {name: 'Mauritius', code: 'MU'},
    {name: 'Mayotte', code: 'YT'},
    {name: 'Mexico', code: 'MX'},
    {name: 'Micronesia, Federated States of', code: 'FM'},
    {name: 'Moldova, Republic of', code: 'MD'},
    {name: 'Monaco', code: 'MC'},
    {name: 'Mongolia', code: 'MN'},
    {name: 'Montserrat', code: 'MS'},
    {name: 'Morocco', code: 'MA'},
    {name: 'Mozambique', code: 'MZ'},
    {name: 'Myanmar', code: 'MM'},
    {name: 'Namibia', code: 'NA'},
    {name: 'Nauru', code: 'NR'},
    {name: 'Nepal', code: 'NP'},
    {name: 'Netherlands', code: 'NL'},
    {name: 'Netherlands Antilles', code: 'AN'},
    {name: 'New Caledonia', code: 'NC'},
    {name: 'New Zealand', code: 'NZ'},
    {name: 'Nicaragua', code: 'NI'},
    {name: 'Niger', code: 'NE'},
    {name: 'Nigeria', code: 'NG'},
    {name: 'Niue', code: 'NU'},
    {name: 'Norfolk Island', code: 'NF'},
    {name: 'Northern Mariana Islands', code: 'MP'},
    {name: 'Norway', code: 'NO'},
    {name: 'Oman', code: 'OM'},
    {name: 'Pakistan', code: 'PK'},
    {name: 'Palau', code: 'PW'},
    {name: 'Palestinian Territory, Occupied', code: 'PS'},
    {name: 'Panama', code: 'PA'},
    {name: 'Papua New Guinea', code: 'PG'},
    {name: 'Paraguay', code: 'PY'},
    {name: 'Peru', code: 'PE'},
    {name: 'Philippines', code: 'PH'},
    {name: 'Pitcairn', code: 'PN'},
    {name: 'Poland', code: 'PL'},
    {name: 'Portugal', code: 'PT'},
    {name: 'Puerto Rico', code: 'PR'},
    {name: 'Qatar', code: 'QA'},
    {name: 'Reunion', code: 'RE'},
    {name: 'Romania', code: 'RO'},
    {name: 'Russian Federation', code: 'RU'},
    {name: 'Rwanda', code: 'RW'},
    {name: 'Saint Helena', code: 'SH'},
    {name: 'Saint Kitts and Nevis', code: 'KN'},
    {name: 'Saint Lucia', code: 'LC'},
    {name: 'Saint Pierre and Miquelon', code: 'PM'},
    {name: 'Saint Vincent and the Grenadines', code: 'VC'},
    {name: 'Samoa', code: 'WS'},
    {name: 'San Marino', code: 'SM'},
    {name: 'Sao Tome and Principe', code: 'ST'},
    {name: 'Saudi Arabia', code: 'SA'},
    {name: 'Senegal', code: 'SN'},
    {name: 'Serbia and Montenegro', code: 'CS'},
    {name: 'Seychelles', code: 'SC'},
    {name: 'Sierra Leone', code: 'SL'},
    {name: 'Singapore', code: 'SG'},
    {name: 'Slovakia', code: 'SK'},
    {name: 'Slovenia', code: 'SI'},
    {name: 'Solomon Islands', code: 'SB'},
    {name: 'Somalia', code: 'SO'},
    {name: 'South Africa', code: 'ZA'},
    {name: 'South Georgia and the South Sandwich Islands', code: 'GS'},
    {name: 'Spain', code: 'ES'},
    {name: 'Sri Lanka', code: 'LK'},
    {name: 'Sudan', code: 'SD'},
    {name: 'Suriname', code: 'SR'},
    {name: 'Svalbard and Jan Mayen', code: 'SJ'},
    {name: 'Swaziland', code: 'SZ'},
    {name: 'Sweden', code: 'SE'},
    {name: 'Switzerland', code: 'CH'},
    {name: 'Syrian Arab Republic', code: 'SY'},
    {name: 'Taiwan, Province of China', code: 'TW'},
    {name: 'Tajikistan', code: 'TJ'},
    {name: 'Tanzania, United Republic of', code: 'TZ'},
    {name: 'Thailand', code: 'TH'},
    {name: 'Timor-Leste', code: 'TL'},
    {name: 'Togo', code: 'TG'},
    {name: 'Tokelau', code: 'TK'},
    {name: 'Tonga', code: 'TO'},
    {name: 'Trinidad and Tobago', code: 'TT'},
    {name: 'Tunisia', code: 'TN'},
    {name: 'Turkey', code: 'TR'},
    {name: 'Turkmenistan', code: 'TM'},
    {name: 'Turks and Caicos Islands', code: 'TC'},
    {name: 'Tuvalu', code: 'TV'},
    {name: 'Uganda', code: 'UG'},
    {name: 'Ukraine', code: 'UA'},
    {name: 'United Arab Emirates', code: 'AE'},
    {name: 'United Kingdom', code: 'GB'},
    {name: 'United States', code: 'US'},
    {name: 'United States Minor Outlying Islands', code: 'UM'},
    {name: 'Uruguay', code: 'UY'},
    {name: 'Uzbekistan', code: 'UZ'},
    {name: 'Vanuatu', code: 'VU'},
    {name: 'Venezuela', code: 'VE'},
    {name: 'Vietnam', code: 'VN'},
    {name: 'Virgin Islands, British', code: 'VG'},
    {name: 'Virgin Islands, U.S.', code: 'VI'},
    {name: 'Wallis and Futuna', code: 'WF'},
    {name: 'Western Sahara', code: 'EH'},
    {name: 'Yemen', code: 'YE'},
    {name: 'Zambia', code: 'ZM'},
    {name: 'Zimbabwe', code: 'ZW'}
  ];

    $scope.images = [];
    
    var fbAuth = ref.getAuth();

    /**Triggered when user clickes upload image icon
      *function will get image data and upload it to database
      *
    $scope.upload = function() {
  
       //  $scope.registered = true;
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            popoverOptions: CameraPopoverOptions,
            targetWidth: 500,
            targetHeight: 500,
            savePhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imageData){
    
            var pic = profiles.$getRecord($localStorage.userkey);
           
            pic.image = imageData;
            $localStorage.imagee = imageData;
            profiles.$save(pic).then(function(ref) 
        {
        }, function(error) 
           {
                console.log("Error:", error);
           });
           
        }, function(error) 
           {
            console.error("ERROR" + error);
           });
    };
    */
    
    $scope.addMedia = function () {
            $scope.hideSheet = $ionicActionSheet.show({
                buttons: [
                    {text: 'Take photo'},
                    {text: 'Photo from library'}
                ],
                titleText: 'Add Images',
                cancelText: 'Cancel',
                buttonClicked: function (index) {
                    $scope.addImage(index);
                }
            });
        }
        function optionsForType(type) {
            var source;
            switch (type) {
                case 0:
                    source = Camera.PictureSourceType.CAMERA;
                    break;
                case 1:
                    source = Camera.PictureSourceType.PHOTOLIBRARY;
                    break;
            }
            return {
                quality: 70,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: source,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
            };
        }
        $scope.addImage = function (type) {
            $scope.hideSheet();
            var options = optionsForType(type);
            $cordovaCamera.getPicture(options).then(function (imageData) {
                $scope.imageAvailability = 1;
                $scope.modify = 1;
                $scope.imgURI = imageData;
                $scope.result = 1;
                
                  var pic = profiles.$getRecord($localStorage.userkey);
           
            pic.image = imageData;
            $localStorage.imagee = imageData;
            profiles.$save(pic).then(function(ref) 
                                     {
        }, function(error) 
           {
                console.log("Error:", error);
           });
                
            }, function (err) {
            });
        }
    
    /**Triggered when user clickes a specific user in all users list
      *function will redirect user to view specific following users
      */
    $scope.view_following = function(){
    
        $state.go('view_following');
    
    }
    
});