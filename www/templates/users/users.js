'Use Strict';
angular.module('app').controller('usersController', function ($scope, $state, $cordovaOauth, $localStorage, $location,$http,$ionicPopup, $firebaseObject, $firebaseArray, $rootScope, Auth, FURL, Utils, toArrayFilter) 
{
  var ref = new Firebase(FURL);
    
  $scope.logOut = function () 
  {
      Auth.logout();
      $location.path("/login");
  }

  $scope.results = $localStorage.name;
  $scope.thisUserId = $localStorage.idd;
    
  Utils.show();
  /* retrieve all users details */
  var users = new Firebase("https://colombo.firebaseio.com/profile");
  var userRef = $firebaseArray(ref.child('profile'));   
  users.once('value', function(snapshot) {
  $scope.members= snapshot.val(); 
      console.log($scope.members);
    Utils.hide();  
  });
    
});


angular.module('app').controller('followingController', function ($scope, $state, $cordovaOauth, $localStorage, $location,$http,$ionicPopup, $firebaseObject, $firebaseArray, $rootScope, Auth, FURL, Utils, toArrayFilter) 
{
  var ref = new Firebase(FURL);
    
  $scope.logOut = function () 
  {
      Auth.logout();
      $location.path("/login");
  }

  $scope.thisUserId = $localStorage.idd;
    
  /* retrieve all users details */
  var users = new Firebase("https://colombo.firebaseio.com/profile/"+$localStorage.userkey+"/following/"); 
  users.once('value', function(snapshot) {
  $scope.following = snapshot.val();
      
      if (!$scope.following) {
            $scope.following_data = true;
      } 
      
  });
   
  $scope.$on('$ionicView.enter', function() {
      
  var users = new Firebase("https://colombo.firebaseio.com/profile/"+$localStorage.userkey+"/following/"); 
  users.once('value', function(snapshot) {
  $scope.following = snapshot.val();  
      
      if (!$scope.following) {
            $scope.following_data = true;
      } 
      
  $scope.$broadcast('scroll.refreshComplete');
  });        
      
  });

});

angular.module('app').controller('otherfollowingController', function ($scope, $state, $stateParams, $cordovaOauth, $localStorage, $location,$http,$ionicPopup, $firebaseObject, $firebaseArray, $rootScope, Auth, FURL, Utils, toArrayFilter,$ionicHistory) 
{
  var ref = new Firebase(FURL);
    
  $scope.logOut = function () 
  {
      Auth.logout();
      $location.path("/login");
  }

  /* user reference id passed to the view */
  $scope.un_id = $stateParams.user_id; 
  $scope.thisUserId = $localStorage.idd;
    
  /* retrieve all users details */
  var users = new Firebase("https://colombo.firebaseio.com/profile/"+$scope.un_id+"/following/"); 
  users.once('value', function(snapshot) {
  $scope.following = snapshot.val();
      
      if (!$scope.following) {
            $scope.following_data = true;
      } 
      
  });
   
  $scope.$on('$ionicView.enter', function() {
      
  var users = new Firebase("https://colombo.firebaseio.com/profile/"+$scope.un_id+"/following/"); 
  users.once('value', function(snapshot) {
  $scope.following = snapshot.val();  
      
      if (!$scope.following) {
            $scope.following_data = true;
      } 
      
  $scope.$broadcast('scroll.refreshComplete');
   });        
      
  });
    
 /**
   *nav bar back button
   */
 $scope.goBack = function(){
    $ionicHistory.goBack();
}

});


angular.module('app').controller('followersController', function ($scope, $state, $cordovaOauth, $localStorage, $location,$http,$ionicPopup, $firebaseObject, $firebaseArray, $rootScope, Auth, FURL, Utils, toArrayFilter) 
{
  var ref = new Firebase(FURL);
    
  $scope.logOut = function () 
  {
      Auth.logout();
      $location.path("/login");
  }

  $scope.thisUserId = $localStorage.idd;
    
  /* retrieve all users details */
  var users = new Firebase("https://colombo.firebaseio.com/profile/"+$localStorage.userkey+"/followers/"); 
  users.once('value', function(snapshot) {
  $scope.follower = snapshot.val();
      
      if (!$scope.follower) {
            $scope.follower_data = true;
      } 
      
  });
   
  $scope.$on('$ionicView.enter', function() {
      
  var users = new Firebase("https://colombo.firebaseio.com/profile/"+$localStorage.userkey+"/followers/"); 
  users.once('value', function(snapshot) {
  $scope.follower = snapshot.val();  
      
      if (!$scope.follower) {
            $scope.follower_data = true;
      } 
      
  $scope.$broadcast('scroll.refreshComplete');
  });        
      
  });

});

angular.module('app').controller('otherfollowersController', function ($scope, $state, $stateParams, $cordovaOauth, $localStorage, $location,$http,$ionicPopup, $firebaseObject, $firebaseArray, $rootScope, Auth, FURL, Utils, toArrayFilter) 
{
  var ref = new Firebase(FURL);
    
  $scope.logOut = function () 
  {
      Auth.logout();
      $location.path("/login");
  }

  /* user reference id passed to the view */
  $scope.un_id = $stateParams.user_id; 
  $scope.thisUserId = $localStorage.idd;
    
  /* retrieve all users details */
  var users = new Firebase("https://colombo.firebaseio.com/profile/"+$scope.un_id+"/followers/"); 
  users.once('value', function(snapshot) {
  $scope.follower = snapshot.val();
      
      if (!$scope.follower) {
            $scope.follower_data = true;
      } 
      
  });
   
  $scope.$on('$ionicView.enter', function() {
      
  var users = new Firebase("https://colombo.firebaseio.com/profile/"+$scope.un_id+"/followers/"); 
  users.once('value', function(snapshot) {
  $scope.follower = snapshot.val();  
      
      if (!$scope.follower) {
            $scope.follower_data = true;
      } 
      
  $scope.$broadcast('scroll.refreshComplete');
  });        
      
  });

});


angular.module('app').controller('blockedController', function ($scope, $state, $cordovaOauth, $localStorage, $location,$http,$ionicPopup, $firebaseObject, $firebaseArray, $rootScope, Auth, FURL, Utils, toArrayFilter) 
{
  var ref = new Firebase(FURL);
    
  $scope.logOut = function () 
  {
      Auth.logout();
      $location.path("/login");
  }

  $scope.thisUserId = $localStorage.idd;
    
  $scope.$on('$ionicView.enter', function() {
      
  var users = new Firebase("https://colombo.firebaseio.com/profile/"+$localStorage.userkey+"/blocked/"); 
  users.once('value', function(snapshot) {
  $scope.blocked = snapshot.val();  
      
      if (!$scope.blocked) {
            $scope.blocked_data = true;
      } 
      
  $scope.$broadcast('scroll.refreshComplete');
  });        
      
  });    

});