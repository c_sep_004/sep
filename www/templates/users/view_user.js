'Use Strict';
angular.module('app').controller('viewuserController', function ($scope, $state, $stateParams, $cordovaOauth, $localStorage, $location,$http,$ionicPopup, $firebaseObject, $firebaseArray, $rootScope, Auth, FURL, Utils, toArrayFilter, $ionicPopover, $ionicLoading, $timeout) 
{
  var ref = new Firebase(FURL);
    
   $ionicLoading.show({
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0,
    template: '<p class="item-icon-left">Loading...<ion-spinner icon="lines"/></p>'
  });    
    
    $timeout(function () {
    $ionicLoading.hide();
    }, 7000);
    
  /* user reference id passed to the view */
  $scope.un_id = $stateParams.unique_id;    

    
  var user_ref = $firebaseObject(ref.child('profile').child($scope.un_id));
  
  $scope.followbtn = false;
  $scope.ufollowbtn = true;
  $scope.blockbtn = false;
  $scope.unblockbtn = true;
    
  /**checking whether the specific user is a already following user 
    *@param logged in user key, clicked users's key
    */
  var check_following = new Firebase("https://colombo.firebaseio.com/profile/"+$localStorage.userkey+"/following");
  check_following.orderByChild("following_id").equalTo($scope.un_id).on("child_added", function(snapshot) {

  $scope.already_following = snapshot.key();
  if($scope.already_following)
  {
    $scope.followbtn = true;
    $scope.ufollowbtn = false;
  }

  });
  
  /**checking whether the specific user is a already blocked user 
    *@param logged in user key, clicked users's key
    */
  var check_blocked = new Firebase("https://colombo.firebaseio.com/profile/"+$localStorage.userkey+"/blocked");
  check_blocked.orderByChild("blocked_id").equalTo($scope.un_id).on("child_added", function(snapshot) {

  $scope.already_blocked = snapshot.key();
  if($scope.already_blocked)
  {
    $scope.blockbtn = true;
    $scope.unblockbtn = false;
  }

  });

  
    
    /* Assign all user details to local variables */
    user_ref.$loaded().then(function(data) 
    {
  
     var search_unblocking = new Firebase("https://colombo.firebaseio.com/profile/"+$scope.un_id+"/blocked/");
     var unblocking_user_ref = search_unblocking.orderByChild("blocked_id").equalTo($localStorage.userkey);
     var unblock_user;

        unblocking_user_ref.once("value", function(response) {
        unblock_user = response.val();
            angular.forEach(unblock_user,function(value,index){
              $scope.dont_display = value.blocked_unique_id;
              if ($scope.dont_display) {
                 var alertPopup = $ionicPopup.alert({
                 title: 'ERROR!',
                 template: 'You have been blocked by this user'
                 });
              }
            })
            
        });
       $scope.user_to_view = user_ref; 
        
        var following_no = new Firebase("https://colombo.firebaseio.com/profile/"+$scope.un_id+"/following/");
        following_no.once("value", function(snapshot) {
            var no = snapshot.numChildren();
            $scope.following_num = no;
        });
        
         var followers_no = new Firebase("https://colombo.firebaseio.com/profile/"+$scope.un_id+"/followers");
         followers_no.once("value", function(snapshot) {
            var num = snapshot.numChildren();
            $scope.followers_num = num;
        });
        
        
        /**
          * To check and get the favourite programs of a perticular user.
          * returns favourites programs list.
          */
        var favourites = new Firebase("https://colombo.firebaseio.com/profile/"+$scope.un_id+"/favourites");
        var favourite_list =  $firebaseObject(favourites);
        favourite_list.$loaded().then(function (data) {
                  
                    $scope.favouriteList = [];
                    
                    angular.forEach(data, function (value, key) {
                        
                        var programref = new Firebase("https://colombo.firebaseio.com/programs").orderByChild("id").equalTo(value);
                        var programs = $firebaseObject(programref);
                        
                        programs.$loaded().then(function (data){
                            
                            angular.forEach(data, function (value, key) {
                                
                                $scope.favouriteList.push(value);
                            });
                            
                            
                        });
                        $scope.list= $scope.favouriteList;
                        
                            
                    }); 
       
            });
        
        
       
    }).catch(function(error) /* If there is a error in loading details */
        {
        
            console.error("Error:", error);
        });
    
    /**Function triggers when user taps follow button in users profile.
      *@param user key id of the specific user who is going to be followed.
      *function will add a node in the logged in users profile called following and add a record of the specific user,
      *who is going to be followed.
      */
    $scope.followUser = function(id){
     
        var loggedin_user = new Firebase("https://colombo.firebaseio.com/profile/"+$localStorage.userkey);
        
        var following_ref = $firebaseObject(ref.child('profile').child(id));
        following_ref.$loaded().then(function(data) 
        {
                
        var follow = $firebaseArray(loggedin_user.child('following'));
        var profile = {
				following_id: id,
                following_name: following_ref.username,
                following_image: following_ref.image,
                following_reg_type : following_ref.reg_type
         };
        follow.$add(profile).then(function(loggedin_user) 
            {
            var id = loggedin_user.key();
            var unique = follow.$getRecord(id);
            unique.following_unique_id = id;
            follow.$save(unique);
        
            $scope.followbtn = true;
            $scope.unfollowbtn = false;
        });            
        }).catch(function(error) /* If there is a error in loading details */
            {
                console.error("Error:", error);
            }); 
        
        
        
        /* Update followeres record of the user */
        
        var follower_user = new Firebase("https://colombo.firebaseio.com/profile/"+id);
        var follower = $firebaseArray(follower_user.child('followers'));
               
        var profile_follower = {
				follower_id: $localStorage.userkey,
                follower_name: $localStorage.name,
                follower_image: $localStorage.imagee,
                follower_reg_type : $localStorage.reg_type
         };
        follower.$add(profile_follower).then(function(follower_user) 
            {
            var id_follower = follower_user.key();
            var unique = follower.$getRecord(id_follower);
            unique.follower_unique_id = id_follower;
            follower.$save(unique);
      
        });            
        
        
        
    };
    
    /**Triggered when user hits following button
      *@param id of the following user
      *function will delete the specific users from logged in users following node
      */
    $scope.unfollowUser = function(val){ 
       
     var search_unfollowing = new Firebase("https://colombo.firebaseio.com/profile/"+$localStorage.userkey+"/following/");
     var unfollowing_user_ref = search_unfollowing.orderByChild("following_id").equalTo(val);
     var unfollow_user;

        unfollowing_user_ref.once("value", function(response) {
        unfollow_user = response.val();
            angular.forEach(unfollow_user,function(value,index){
              var unfollower = value.following_unique_id;
              search_unfollowing.child(unfollower).remove();
            })
    
        });
          
       $scope.followbtn = false;
       $scope.ufollowbtn = true;
   };
    
   /**Funtion triggers when user taps block button in users profile.
      *@param user key id of the specific user who is going to be blocked.
      *function will add a node in the logged in users profile called blocked and add a record of the specific user,
      *who is going to be blocked.
      */
   $scope.blockUser = function(block_id){
     
        var loggedin_user = new Firebase("https://colombo.firebaseio.com/profile/"+$localStorage.userkey);
        var block = $firebaseArray(loggedin_user.child('blocked'));
        var blocked_ref = $firebaseObject(ref.child('profile').child(block_id));
        blocked_ref.$loaded().then(function(data) 
        {
        var profile = {
				blocked_id: block_id,
                blocked_name: blocked_ref.username,
                blocked_image: blocked_ref.image,
                blocked_reg_type: blocked_ref.reg_type
         };
        block.$add(profile).then(function(loggedin_user) 
            {
            var id = loggedin_user.key();
            var unique = block.$getRecord(id);
            unique.blocked_unique_id = id;
            block.$save(unique);
           console.log("blocked");
            $scope.blockbtn = true;
            $scope.unblockbtn = false;
        });            
        }).catch(function(error) /* If there is a error in loading details */
            {
                console.error("Error:", error);
            });  
    };
    
     /**Triggered when user hits unblock button
      *@param id of the blocked user
      *function will delete the specific users from logged in users blocked node
      */
    $scope.unblockUser = function(val){ 
       
     var search_unblocking = new Firebase("https://colombo.firebaseio.com/profile/"+$localStorage.userkey+"/blocked/");
     var unblocking_user_ref = search_unblocking.orderByChild("blocked_id").equalTo(val);
     var unblock_user;

        unblocking_user_ref.once("value", function(response) {
        unblock_user = response.val();
            angular.forEach(unblock_user,function(value,index){
              var unblocker = value.blocked_unique_id;
              search_unblocking.child(unblocker).remove();
            })
    
        });
          
        $scope.blockbtn = false;
        $scope.unblockbtn = true;
   };
    
      var template = '<ion-popover-view style="height: 70px"><ion-content><div class="list"><ion-item class="item" href="#/app/view_user/{{user_to_view.unique_id}}" ng-click="reported(user_to_view.unique_id)">report user</ion-item></div></ion-content></ion-popover-view>';

  $scope.popover = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });
    
    $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
    
    /**Triggered when report user button is clicked
      * @param int id of the reported user
      * function will create a node in database for the reported user so that admin panel can view it from their system
      */
    $scope.reported = function(val){
        $scope.data = {};
        var alertPopup = $ionicPopup.show({
            title: 'Help Us Understand What is Happening',
            template: '<textarea rows="6" placeholder="why do you want to report this person?" ng-model="data.choice"></textarea> ',
            scope: $scope,
            buttons: [
                        { text: 'Cancel' },
                            {
                                text: '<b>Ok</b>',
                                type: 'button-positive',
                                onTap: function(e) 
                                {
                                    if (!$scope.data.choice) {
                                        e.preventDefault();                                   
                                    } 
                                    else {  
                                        var profileRef = $firebaseArray(ref.child('reported'));
                                        var profile = {
		                                              reported_by:$localStorage.userkey,
                                                      user_id:val,
                                                      reason:$scope.data.choice
                                                      };
                                            profileRef.$add(profile).then(function(ref) 
                                            {
		                                      var id = ref.key();
                                              var alertPopup = $ionicPopup.alert({
                                                    title: 'User Reported!',
                                                    template: 'You have reported this user. We are sorry that you have had this experience.'
                                                });
		                                    });
                                          }
                                }
                            },
                     ]
          })
     };
    
});