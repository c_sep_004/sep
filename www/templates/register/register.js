/** This module will handle the registration functions */

'Use Strict';
angular.module('app').controller('registerController', function ($scope, $state,$cordovaOauth, $localStorage, $location,$http,$ionicPopup, $firebaseObject, Auth, FURL, Utils) 
{
  /** Triggered when user hits register button
    * Function will retrieve the information user provided and send to the auth.js to do the registration part.
    * User will be notified once the registration is done.
    */
  $scope.register = function(user) 
  {
    /* Check whether the user is a defined user */
    if(angular.isDefined(user))
    {
      Utils.show();
      /* Registering user */
      Auth.register(user).then(function() 
        {
          Utils.hide();
          Utils.alertshow("Successfully","The User was Successfully Created.");
          $location.path('/');
        }, function(err) 
           {
             Utils.hide();
             Utils.errMessage(err);
           });
    }
  };

});
